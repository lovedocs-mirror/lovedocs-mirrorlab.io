#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright 2018 Pedro Gimeno Fortea
"""Generate list of pages for use in down.py."""

import requests
import sys
import urllib.parse
from datetime import datetime


proxies = {
    'http': 'http://127.0.0.1:8118',
    'https': 'http://127.0.0.1:8118'
}

excludeNS = frozenset(('Talk', 'User talk', 'LOVE talk', 'File talk',
                       'Template talk', 'Help talk', 'Category talk',
                       'Property talk', 'Type talk', 'Concept talk',
                       'Tutorial talk', 'MediaWiki talk', 'Media', 'Special'))

nsURL = 'https://www.love2d.org/w/api.php?action=query' \
        '&meta=siteinfo&siprop=namespaces&format=json'

allURL = 'https://love2d.org/w/api.php?action=query&list=allpages' \
         '&aplimit=200&format=json'


recentURL = 'https://love2d.org/w/api.php?action=query&list=recentchanges' \
            '&rcend=%s&rcprop=title%7Cids&rclimit=100&format=json'

pageList = []


def save_file(saveloc, text):
    """Save a string at saveloc."""
    f = open(saveloc, 'w')
    try:
        f.write(text)
    finally:
        f.close()


def load_file(loc):
    """Read a file returning it as a string."""
    f = open(loc, 'r')
    try:
        txt = f.read()
    finally:
        f.close()

    return txt


def get_namespace(ns, nsname):
    """Retrieve a namespace."""
    url = allURL
    if ns:
        url = url + '&apnamespace=' + ns

    start = ''
    req = requests.get(url, proxies=proxies)
    while req.status_code == 200:
        pages = req.json()
        if 'query' not in pages:
            sys.stdout.write("Warning!*********\n"
                             + repr(pages) + "\n" + nsname + "\n")
        for k in pages['query']['allpages']:
            pageList.append(k['title'])

        if 'query-continue' not in pages:
            break
        start = pages['query-continue']['allpages']['apcontinue']
        sys.stdout.write('Reading from %s...\n' % start)
        req = requests.get(url + '&apfrom=' + urllib.parse.quote_plus(start),
                           proxies=proxies)


def main():
    """Main."""
    assert len(sys.argv) > 1, "Argument needed: basename"
    timestamp = datetime.utcnow().isoformat() + 'Z'
    nsquery = requests.get(nsURL, proxies=proxies)
    if nsquery.status_code != 200:
        raise Exception('Error retrieving namespaces. Result code: '
                        + str(nsquery.status_code))
    nspaces = nsquery.json()['query']['namespaces']
    nspaces = dict([(nspaces[k]['*'], k) for k in nspaces
                    if nspaces[k]['*'] not in excludeNS])

    for k in nspaces:
        get_namespace(nspaces[k], k)

    pageList.sort()
    save_file(sys.argv[1] + '.lst', '\n'.join(pageList)
              + ('\n' if pageList else ''))
    save_file(sys.argv[1] + '.ts', timestamp)


try:
    result = main()
except KeyboardInterrupt:
    result = 1

if result:
    sys.exit(result)
