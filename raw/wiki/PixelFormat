<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>PixelFormat - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="PixelFormat" href="/w/index.php?title=Special:ExportRDF/PixelFormat&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/PixelFormat" />
<link rel="copyright" href="https://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=ext.geshi.language.lua%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"PixelFormat","wgTitle":"PixelFormat","wgCurRevisionId":25271,"wgRevisionId":25271,"wgArticleId":6314,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Enums"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"PixelFormat","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.style","mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-PixelFormat skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-main" class="selected"><a href="/wiki/PixelFormat" title="View the content page [c]" accesskey="c">Page</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=Talk:PixelFormat&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=PixelFormat&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=PixelFormat&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">PixelFormat</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><table class="new-section" bgcolor="#d0d0ff" style="margin-top: 1em; padding: 3px; border-radius:5px; -moz-border-radius:5px;" data-newin="&#91;&#91;11.0]]">
<tr>
<td><i> <b>Available since LÖVE <a href="/wiki/11.0" title="11.0">11.0</a></b> </i>
</td></tr>
<tr>
<td> <small>This enum replaces <a href="/wiki/CanvasFormat" title="CanvasFormat">CanvasFormat</a> and <a href="/wiki/CompressedImageFormat" title="CompressedImageFormat">CompressedImageFormat</a>.</small>
</td></tr></table>
<p>Pixel formats for <a href="/wiki/Texture" title="Texture">Textures</a>, <a href="/wiki/ImageData" title="ImageData">ImageData</a>, and <a href="/wiki/CompressedImageData" title="CompressedImageData">CompressedImageData</a>.
</p>
<h2><span class="mw-headline" id="Normal_color_formats">Normal color formats</span></h2>
<h3><span class="mw-headline" id="Constants">Constants</span></h3>
<table class="wikitable">
<tr>
<th> Name
</th>
<th> Components
</th>
<th> Bits per pixel
</th>
<th> Range
</th>
<th> Usable with <a href="/wiki/Canvas" title="Canvas">Canvases</a>
</th>
<th> Usable with <a href="/wiki/ImageData" title="ImageData">ImageData</a>
</th>
<th> Note(s)
</th></tr>
<tr>
<td>normal
</td>
<td>4
</td>
<td>32
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>
</td>
<td>Alias for <code>rgba8</code>, or <code>srgba8</code> if <a href="/wiki/love.graphics.isGammaCorrect" title="love.graphics.isGammaCorrect">gamma-correct rendering</a> is enabled.
</td></tr>
<tr>
<td>r8
</td>
<td>1
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rg8
</td>
<td>2
</td>
<td>16
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgba8
</td>
<td>4
</td>
<td>32
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>Yes
</td>
<td>
</td></tr>
<tr>
<td>srgba8
</td>
<td>4
</td>
<td>32
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>
</td>
<td><a href="/wiki/love.graphics.isGammaCorrect" title="love.graphics.isGammaCorrect">gamma-correct</a> version of rgba8.
</td></tr>
<tr>
<td>r16
</td>
<td>1
</td>
<td>16
</td>
<td>[0, 1]
</td>
<td>
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rg16
</td>
<td>2
</td>
<td>32
</td>
<td>[0, 1]
</td>
<td>
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgba16
</td>
<td>4
</td>
<td>64
</td>
<td>[0, 1]
</td>
<td>
</td>
<td>Yes
</td>
<td>
</td></tr>
<tr>
<td>r16f
</td>
<td>1
</td>
<td>16
</td>
<td>[-65504, +65504]*
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rg16f
</td>
<td>2
</td>
<td>32
</td>
<td>[-65504, +65504]*
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgba16f
</td>
<td>4
</td>
<td>64
</td>
<td>[-65504, +65504]*
</td>
<td>Yes
</td>
<td>Yes
</td>
<td>
</td></tr>
<tr>
<td>r32f
</td>
<td>1
</td>
<td>32
</td>
<td>[-3.4028235e38, 3.4028235e38]*
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rg32f
</td>
<td>2
</td>
<td>64
</td>
<td>[-3.4028235e38, 3.4028235e38]*
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgba32f
</td>
<td>4
</td>
<td>128
</td>
<td>[-3.4028235e38, 3.4028235e38]*
</td>
<td>Yes
</td>
<td>Yes
</td>
<td>
</td></tr>
<tr>
<td>rgba4
</td>
<td>4
</td>
<td>16
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgb5a1
</td>
<td>4
</td>
<td>16
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgb565
</td>
<td>3
</td>
<td>16
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rgb10a2
</td>
<td>4
</td>
<td>32
</td>
<td>[0, 1]
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr>
<tr>
<td>rg11b10f
</td>
<td>3
</td>
<td>32
</td>
<td>[0, 65024]**
</td>
<td>Yes
</td>
<td>11.3
</td>
<td>
</td></tr></table>
<p>* -infinity and +infinity are also valid values.<br />
** +infinity is also a valid value.
</p>
<h2><span class="mw-headline" id="Depth_.2F_stencil_formats">Depth / stencil formats</span></h2>
<p>All depth and stencil pixel formats are only usable in <a href="/wiki/Canvas" title="Canvas">Canvases</a>.
</p><p>They are <a href="/wiki/Texture:isReadable" title="Texture:isReadable">non-readable</a> by default, and Canvases with a depth/stencil format created with the <a href="/wiki/love.graphics.newCanvas" title="love.graphics.newCanvas">readable flag</a> can only access the depth values of their pixels in shaders (stencil values are not readable no matter what).
</p>
<h3><span class="mw-headline" id="Constants_2">Constants</span></h3>
<table class="wikitable">
<tr>
<th> Name
</th>
<th> Bits per pixel
</th>
<th> Has depth
</th>
<th> Has stencil
</th>
<th> Note(s)
</th></tr>
<tr>
<td>stencil8
</td>
<td>8
</td>
<td>
</td>
<td>Yes
</td>
<td>
</td></tr>
<tr>
<td>depth16
</td>
<td>16
</td>
<td>Yes
</td>
<td>
</td>
<td>
</td></tr>
<tr>
<td>depth24
</td>
<td>24
</td>
<td>Yes
</td>
<td>
</td>
<td>
</td></tr>
<tr>
<td>depth32f
</td>
<td>32
</td>
<td>Yes
</td>
<td>
</td>
<td>
</td></tr>
<tr>
<td>depth24stencil8
</td>
<td>32
</td>
<td>Yes
</td>
<td>Yes
</td>
<td>
</td></tr>
<tr>
<td>depth32fstencil8
</td>
<td>40
</td>
<td>Yes
</td>
<td>Yes
</td>
<td>
</td></tr></table>
<h2><span class="mw-headline" id="Compressed_formats">Compressed formats</span></h2>
<p>All compressed pixel formats are only usable in <a href="/wiki/Image" title="Image">Images</a> via <a href="/wiki/CompressedImageData" title="CompressedImageData">CompressedImageData</a> (compressed textures).
</p><p>Unlike regular color formats, these stay compressed in RAM and VRAM. This is good for saving memory space as well as improving performance, since the graphics card will be able to keep more of the image's pixels in its fast-access cache when drawing it.
</p>
<h3><span class="mw-headline" id="Constants_.28desktop_GPUs.29">Constants (desktop GPUs)</span></h3>
<table class="wikitable">
<tr>
<th> Name
</th>
<th> Components
</th>
<th> Bits per pixel
</th>
<th> Range
</th>
<th> Note(s)
</th></tr>
<tr>
<td>DXT1
</td>
<td>3
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Suitable for fully opaque images on desktop systems.
</td></tr>
<tr>
<td>DXT3
</td>
<td>4
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Smooth variations in opacity do not mix well with this format. DXT1 or DXT5 is generally better in every situation.
</td></tr>
<tr>
<td>DXT5
</td>
<td>4
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Recommended for images with varying opacity on desktop systems.
</td></tr>
<tr>
<td>BC4
</td>
<td>1
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Also known as 3Dc+ or ATI1. Stores just the red channel.
</td></tr>
<tr>
<td>BC4s
</td>
<td>1
</td>
<td>4
</td>
<td>[-1, 1]
</td>
<td>Less precision than BC4 but allows negative numbers.
</td></tr>
<tr>
<td>BC5
</td>
<td>2
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Also known as 3Dc or ATI2. Often used for normal maps on desktop systems.
</td></tr>
<tr>
<td>BC5s
</td>
<td>2
</td>
<td>8
</td>
<td>[-1, 1]
</td>
<td>Less precision than BC5 but allows negative numbers. Often used for normal maps on desktop systems.
</td></tr>
<tr>
<td>BC6h
</td>
<td>3
</td>
<td>8
</td>
<td>[0, +infinity]
</td>
<td>Stores <a rel="nofollow" class="external text" href="https://en.wikipedia.org/wiki/Half-precision_floating-point_format">half-precision</a> floating point RGB data. Suitable for HDR images on desktop systems.
</td></tr>
<tr>
<td>BC6hs
</td>
<td>3
</td>
<td>8
</td>
<td>[-infinity, +infinity]
</td>
<td>Less precision than BC6h but allows negative numbers.
</td></tr>
<tr>
<td>BC7
</td>
<td>4
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Very good at representing opaque or transparent images, but requires a DX11 / OpenGL 4-capable GPU.
</td></tr></table>
<h3><span class="mw-headline" id="Constants_.28mobile_GPUs.29">Constants (mobile GPUs)</span></h3>
<table class="wikitable">
<tr>
<th> Name
</th>
<th> Components
</th>
<th> Bits per pixel
</th>
<th> Range
</th>
<th> Note(s)
</th></tr>
<tr>
<td>ETC1
</td>
<td>3
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Suitable for fully opaque images on older Android devices.
</td></tr>
<tr>
<td>ETC2rgb
</td>
<td>3
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Suitable for fully opaque images on newer mobile devices
</td></tr>
<tr>
<td>ETC2rgba
</td>
<td>4
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Recommended for images with varying opacity on newer mobile devices.
</td></tr>
<tr>
<td>ETC2rgba1
</td>
<td>4
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>RGBA variant of the ETC2 format where pixels are either fully transparent or fully opaque.
</td></tr>
<tr>
<td>EACr
</td>
<td>1
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Stores just the red channel.
</td></tr>
<tr>
<td>EACrs
</td>
<td>1
</td>
<td>4
</td>
<td>[-1, 1]
</td>
<td>Less precision than EACr but allows negative numbers.
</td></tr>
<tr>
<td>EACrg
</td>
<td>2
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>Stores red and green channels.
</td></tr>
<tr>
<td>EACrgs
</td>
<td>2
</td>
<td>8
</td>
<td>[-1, 1]
</td>
<td>Less precision than EACrg but allows negative numbers.
</td></tr>
<tr>
<td>PVR1rgb2
</td>
<td>3
</td>
<td>2
</td>
<td>[0, 1]
</td>
<td>Images using this format must be square and power-of-two sized.
</td></tr>
<tr>
<td>PVR1rgb4
</td>
<td>3
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Images using this format must be square and power-of-two sized.
</td></tr>
<tr>
<td>PVR1rgba2
</td>
<td>4
</td>
<td>2
</td>
<td>[0, 1]
</td>
<td>Images using this format must be square and power-of-two sized.
</td></tr>
<tr>
<td>PVR1rgba4
</td>
<td>4
</td>
<td>4
</td>
<td>[0, 1]
</td>
<td>Images using this format must be square and power-of-two sized.
</td></tr>
<tr>
<td>ASTC4x4
</td>
<td>4
</td>
<td>8
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC5x4
</td>
<td>4
</td>
<td>6.4
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC5x5
</td>
<td>4
</td>
<td>5.12
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC6x5
</td>
<td>4
</td>
<td>4.27
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC6x6
</td>
<td>4
</td>
<td>3.56
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC8x5
</td>
<td>4
</td>
<td>3.2
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC8x6
</td>
<td>4
</td>
<td>2.67
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC8x8
</td>
<td>4
</td>
<td>2
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC10x5
</td>
<td>4
</td>
<td>2.56
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC10x6
</td>
<td>4
</td>
<td>2.13
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC10x8
</td>
<td>4
</td>
<td>1.6
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC10x10
</td>
<td>4
</td>
<td>1.28
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC12x10
</td>
<td>4
</td>
<td>1.07
</td>
<td>[0, 1]
</td>
<td>
</td></tr>
<tr>
<td>ASTC12x12
</td>
<td>4
</td>
<td>0.89
</td>
<td>[0, 1]
</td>
<td>
</td></tr></table>
<h2><span class="mw-headline" id="Notes">Notes</span></h2>
<p>Not all formats are supported in love.graphics <a href="/wiki/Image" title="Image">Images</a> and <a href="/wiki/Canvas" title="Canvas">Canvases</a> on all systems, although the DXT compressed image formats have close to 100% support on desktop operating systems.
</p><p>The BC4 and BC5 formats are supported on systems with DirectX 10 / OpenGL 3-capable desktop hardware and drivers. The BC6H and BC7 formats are only supported on desktop systems with DirectX 11 / OpenGL 4-capable hardware and very recent drivers. macOS does not support BC6H or BC7 at all currently.
</p><p>ETC1 is supported by <i>most</i> Android devices, as well as the iPhone 5s and newer.
</p><p>The ETC2 and EAC formats are supported by the iPhone 5s and newer, all OpenGL ES 3-capable Android devices, and OpenGL 4.3-capable desktop GPUs.
</p><p>The PVR1 formats are supported by all iOS devices, as well as the small number of Android devices which have PowerVR GPUs.
</p><p>ASTC is supported by new mobile devices (e.g. the iPhone 6 and newer), Android devices which have Adreno 4xx (and later) GPUs, and Skylake (and newer) integrated Intel GPUs. It has a variety of variants to allow for picking the most compressed possible one that doesn't have any noticeable compression artifacts, for a given texture.
</p><p>Use <a href="/wiki/love.graphics.getCanvasFormats" title="love.graphics.getCanvasFormats">love.graphics.getCanvasFormats</a> and <a href="/wiki/love.graphics.getImageFormats" title="love.graphics.getImageFormats">love.graphics.getImageFormats</a> to check for Canvas and Image support, respectively:
</p>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">local</span> supportedformats <span class="sy0">=</span> love<span class="sy0">.</span>graphics<span class="sy0">.</span>getImageFormats<span class="br0">&#40;</span><span class="br0">&#41;</span>
&#160;
<span class="kw1">if</span> <span class="kw2">not</span> supportedformats<span class="br0">&#91;</span><span class="st0">&quot;DXT5&quot;</span><span class="br0">&#93;</span> <span class="kw1">then</span>
    <span class="co1">-- Can't load CompressedImageData with the DXT5 format into images!</span>
    <span class="co1">-- On some Linux systems with Mesa drivers, the user will need to install a &quot;libtxc-dxtn&quot; package because the DXT (aka S3TC) formats used to be patented.</span>
    <span class="co1">-- Support for DXT formats on all other desktop drivers is pretty much guaranteed.</span>
<span class="kw1">end</span>
&#160;
<span class="kw1">if</span> <span class="kw2">not</span> supportedformats<span class="br0">&#91;</span><span class="st0">&quot;BC5&quot;</span><span class="br0">&#93;</span> <span class="kw1">then</span>
    <span class="co1">-- Can't load CompressedImageData with the BC5 format into images!</span>
    <span class="co1">-- The user likely doesn't have a video card capable of using that format.</span>
<span class="kw1">end</span></pre></div></div>
<h2><span class="mw-headline" id="See_Also">See Also</span></h2>
<ul><li> <a href="/wiki/love.image" title="love.image">love.image</a></li>
<li> <a href="/wiki/love.graphics" title="love.graphics">love.graphics</a></li>
<li> <a href="/wiki/love.font" title="love.font">love.font</a></li>
<li> <a href="/wiki/love.image.newImageData" title="love.image.newImageData">love.image.newImageData</a></li>
<li> <a href="/wiki/love.graphics.getImageFormats" title="love.graphics.getImageFormats">love.graphics.getImageFormats</a></li>
<li> <a href="/wiki/love.graphics.getCanvasFormats" title="love.graphics.getCanvasFormats">love.graphics.getCanvasFormats</a></li>
<li> <a href="/wiki/love.graphics.newCanvas" title="love.graphics.newCanvas">love.graphics.newCanvas</a></li>
<li> <a href="/wiki/Texture:getFormat" title="Texture:getFormat">Texture:getFormat</a></li>
<li> <a href="/wiki/ImageData:getFormat" title="ImageData:getFormat">ImageData:getFormat</a></li>
<li> <a href="/wiki/CompressedImageData:getFormat" title="CompressedImageData:getFormat">CompressedImageData:getFormat</a></li>
<li> <a href="/wiki/GlyphData:getFormat" title="GlyphData:getFormat">GlyphData:getFormat</a></li></ul>
<p><br />
</p>
<h2><span class="mw-headline" id="Other_Languages">Other Languages</span></h2>
<div class="i18n">
<p><a href="/w/index.php?title=PixelFormat_(Dansk)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Dansk) (page does not exist)">Dansk</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Deutsch)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Deutsch) (page does not exist)">Deutsch</a>&#160;&#8211;
<strong class="selflink">English</strong>&#160;&#8211; 
<a href="/w/index.php?title=PixelFormat_(Espa%C3%B1ol)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Español) (page does not exist)">Español</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Fran%C3%A7ais)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Français) (page does not exist)">Français</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Indonesia)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Indonesia) (page does not exist)">Indonesia</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Italiano)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Italiano) (page does not exist)">Italiano</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Lietuvi%C5%A1kai)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Lietuviškai) (page does not exist)">Lietuviškai</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Magyar)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Magyar) (page does not exist)">Magyar</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Nederlands)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Nederlands) (page does not exist)">Nederlands</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Polski)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Polski) (page does not exist)">Polski</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Portugu%C3%AAs)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Português) (page does not exist)">Português</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Rom%C3%A2n%C4%83)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Română) (page does not exist)">Română</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Slovensk%C3%BD)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Slovenský) (page does not exist)">Slovenský</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Suomi)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Suomi) (page does not exist)">Suomi</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Svenska)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Svenska) (page does not exist)">Svenska</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(T%C3%BCrk%C3%A7e)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Türkçe) (page does not exist)">Türkçe</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%C4%8Cesky)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Česky) (page does not exist)">Česky</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%CE%95%CE%BB%CE%BB%CE%B7%CE%BD%CE%B9%CE%BA%CE%AC)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Ελληνικά) (page does not exist)">Ελληνικά</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Български) (page does not exist)">Български</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Русский) (page does not exist)">Русский</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%D0%A1%D1%80%D0%BF%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Српски) (page does not exist)">Српски</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D1%81%D1%8C%D0%BA%D0%B0)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Українська) (page does not exist)">Українська</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%D7%A2%D7%91%D7%A8%D7%99%D7%AA)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (עברית) (page does not exist)">עברית</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%E0%B9%84%E0%B8%97%E0%B8%A2)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (ไทย) (page does not exist)">ไทย</a>&#160;&#8211;
<a href="/wiki/PixelFormat_(%E6%97%A5%E6%9C%AC%E8%AA%9E)" title="PixelFormat (日本語)">日本語</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (正體中文) (page does not exist)">正體中文</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (简体中文) (page does not exist)">简体中文</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(Ti%E1%BA%BFng_Vi%E1%BB%87t)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (Tiếng Việt) (page does not exist)">Tiếng Việt</a>&#160;&#8211;
<a href="/w/index.php?title=PixelFormat_(%ED%95%9C%EA%B5%AD%EC%96%B4)&amp;action=edit&amp;redlink=1" class="new" title="PixelFormat (한국어) (page does not exist)">한국어</a> 
<br />
<span style="text-align:right;"><i><a href="/wiki/Help:i18n" title="Help:i18n">More info</a></i></span>
</p>
</div>

<!-- 
NewPP limit report
CPU time usage: 0.100 seconds
Real time usage: 0.118 seconds
Preprocessor visited node count: 335/1000000
Preprocessor generated node count: 951/1000000
Post‐expand include size: 3774/2097152 bytes
Template argument size: 1370/2097152 bytes
Highest expansion depth: 5/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key love2d_wiki:pcache:idhash:6314-0!*!0!!*!*!* and timestamp 20200521130316 and revision id 25271
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=PixelFormat&amp;oldid=25271">https://love2d.org/w/index.php?title=PixelFormat&amp;oldid=25271</a>"</div>
					<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Enums" title="Category:Enums">Enums</a></li></ul></div></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=PixelFormat" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://github.com/love2d/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/PixelFormat" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/PixelFormat" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=PixelFormat&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=PixelFormat&amp;oldid=25271" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=PixelFormat&amp;action=info">Page information</a></li>
											<li id="t-smwbrowselink"><a href="/wiki/Special:Browse/PixelFormat" rel="smw-browse">Browse properties</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="https://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 22 April 2020, at 11:59.</li>
									<li id="viewcount">This page has been accessed 13,413 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="https://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.tooltips","mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":90});
}</script></body></html>
