<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>uLove Compliance conf.lua - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="uLove Compliance conf.lua" href="/w/index.php?title=Special:ExportRDF/uLove_Compliance_conf.lua&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/uLove_Compliance_conf.lua" />
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=ext.geshi.language.lua%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"uLove_Compliance_conf.lua","wgTitle":"uLove Compliance conf.lua","wgCurRevisionId":19089,"wgRevisionId":19089,"wgArticleId":691,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Snippets"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"uLove_Compliance_conf.lua","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.style","mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-uLove_Compliance_conf_lua skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-main" class="selected"><a href="/wiki/uLove_Compliance_conf.lua" title="View the content page [c]" accesskey="c">Page</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=Talk:uLove_Compliance_conf.lua&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=uLove_Compliance_conf.lua&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=uLove_Compliance_conf.lua&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">uLove Compliance conf.lua</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><table class="notice" bgcolor="gold" style="border-style:solid;border-width:1px;-moz-border-radius:3px;border-radius:3px;" align="center" width="80%">
<tr>
<td width="1"> <a href="/wiki/File:O.png" class="image"><img alt="O.png" src="/w/images/0/09/O.png" width="64" height="64" /></a>
</td>
<td>This page has no more real relevance to Löve.
</td>
<td width="1">&#160;
</td></tr></table><br />
<h2><span class="mw-headline" id="Introduction">Introduction</span></h2>
<p>This <a href="/wiki/conf.lua" title="conf.lua" class="mw-redirect">conf.lua</a> file can be used to test minimal compliance with the proposed <a href="/wiki/uLove" title="uLove">uLove</a> standard.
</p><p>This is by no means a complete workout of your code, and you should refer to the <a href="/wiki/uLove" title="uLove">Standard</a> itself to be sure your code is compliant. but this conf is a useful benchmark, if it runs with this, then you are in with a good shot.
</p>
<h2><span class="mw-headline" id="uLove_conf.lua">uLove conf.lua</span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="co1">-- conf.lua</span>
<span class="co1">-- use this in place of your games normal conf.lua to see how it deals</span>
<span class="co1">-- with some common, or likely, limitations of uLove-targeted platforms.</span>
<span class="co1">--</span>
<span class="co1">-- Bare in mind, this is by no means a complete test, but its a minimal </span>
<span class="co1">-- benchmark; if it can't run under this, then you are definitely going to</span>
<span class="co1">-- have problems.</span>
<span class="kw1">function</span> love<span class="sy0">.</span>conf<span class="br0">&#40;</span>t<span class="br0">&#41;</span>
	t<span class="sy0">.</span>title				<span class="sy0">=</span> <span class="st0">&quot;uLove Minimal Compliance Test&quot;</span>
	t<span class="sy0">.</span>author			<span class="sy0">=</span> <span class="st0">&quot;Textmode (DMB)&quot;</span>
	t<span class="sy0">.</span>version			<span class="sy0">=</span> <span class="nu0">60</span>
	t<span class="sy0">.</span>console			<span class="sy0">=</span> <span class="kw4">false</span>
&#160;
&#160;
	t<span class="sy0">.</span>screen<span class="sy0">.</span>width		<span class="sy0">=</span> <span class="nu0">320</span>
	t<span class="sy0">.</span>screen<span class="sy0">.</span>height		<span class="sy0">=</span> <span class="nu0">240</span>
	t<span class="sy0">.</span>screen<span class="sy0">.</span>fullscreen	<span class="sy0">=</span> <span class="kw4">false</span>						<span class="co1">-- this is likely to be true</span>
													<span class="co1">-- on an uLove target, but few modern systems</span>
													<span class="co1">-- actually support this res ...so, yeah.</span>
&#160;
	t<span class="sy0">.</span>screen<span class="sy0">.</span>vsync		<span class="sy0">=</span> <span class="kw4">true</span>						<span class="co1">-- actually could go either way on a uLove</span>
													<span class="co1">-- platform, but anything to bring your</span>
													<span class="co1">-- FPS down, lest you forget that most </span>
													<span class="co1">-- uLove targets are likely to be sub-500MHz</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>joystick	<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>audio		<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>keyboard	<span class="sy0">=</span> <span class="kw4">true</span>						<span class="co1">--  if you actually have a game pad, you </span>
													<span class="co1">-- should make sure your game can be played</span>
													<span class="co1">-- *entirely* using that that. and no cheating with</span>
													<span class="co1">-- &quot;pro&quot; gamepads; we're talking one D-pad, A, B,</span>
													<span class="co1">-- R, L, Start, and Select. and that's generous.</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>event		<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>image		<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>graphics	<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>timer		<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>mouse		<span class="sy0">=</span> <span class="kw4">false</span>						<span class="co1">-- there are far more portables without</span>
													<span class="co1">-- mouse-like input, than those with.</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>sound		<span class="sy0">=</span> <span class="kw4">true</span>
	t<span class="sy0">.</span>modules<span class="sy0">.</span>physics	<span class="sy0">=</span> <span class="kw4">false</span>						<span class="co1">-- aw, you thought uLove platforms were powerful</span>
													<span class="co1">-- enough to run physics, that's cute ^_^</span>
<span class="kw1">end</span></pre></div></div>
<p><br />
</p>
<!-- 
NewPP limit report
CPU time usage: 0.040 seconds
Real time usage: 0.043 seconds
Preprocessor visited node count: 18/1000000
Preprocessor generated node count: 87/1000000
Post‐expand include size: 334/2097152 bytes
Template argument size: 46/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key love2d_wiki:pcache:idhash:691-0!*!0!!*!5!* and timestamp 20180619143209 and revision id 19089
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=uLove_Compliance_conf.lua&amp;oldid=19089">https://love2d.org/w/index.php?title=uLove_Compliance_conf.lua&amp;oldid=19089</a>"</div>
					<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Snippets" title="Category:Snippets">Snippets</a></li></ul></div></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=uLove+Compliance+conf.lua" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://bitbucket.org/rude/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/uLove_Compliance_conf.lua" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/uLove_Compliance_conf.lua" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=uLove_Compliance_conf.lua&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=uLove_Compliance_conf.lua&amp;oldid=19089" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=uLove_Compliance_conf.lua&amp;action=info">Page information</a></li>
											<li id="t-smwbrowselink"><a href="/wiki/Special:Browse/uLove_Compliance_conf.lua" rel="smw-browse">Browse properties</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="http://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 12 November 2016, at 02:39.</li>
									<li id="viewcount">This page has been accessed 9,921 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.tooltips","mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":88});
}</script></body></html>
