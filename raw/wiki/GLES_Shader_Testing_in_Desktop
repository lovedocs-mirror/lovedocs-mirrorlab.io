<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>GLES Shader Testing in Desktop - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="GLES Shader Testing in Desktop" href="/w/index.php?title=Special:ExportRDF/GLES_Shader_Testing_in_Desktop&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/GLES_Shader_Testing_in_Desktop" />
<link rel="copyright" href="https://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"GLES_Shader_Testing_in_Desktop","wgTitle":"GLES Shader Testing in Desktop","wgCurRevisionId":25539,"wgRevisionId":25539,"wgArticleId":6275,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorials"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"GLES_Shader_Testing_in_Desktop","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.style","mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-GLES_Shader_Testing_in_Desktop skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-main" class="selected"><a href="/wiki/GLES_Shader_Testing_in_Desktop" title="View the content page [c]" accesskey="c">Page</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=Talk:GLES_Shader_Testing_in_Desktop&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">GLES Shader Testing in Desktop</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><p>Writing shaders to support mobile devices might be not trivial. You may wondering is there a way to test shader in mobile devices without really using mobile device.
</p><p>The answer is yes, there's a way.
</p><p>Desktop GPU nowadays (even integrated one) should supports creating OpenGL ES context, which means all shaders must be GLSL ES/ESSL compatible. We can make LOVE to use OpenGL ES context by using environment variable <code>LOVE_GRAPHICS_USE_OPENGLES=1</code>.
</p><p>Note that this guide is mostly Windows oriented. For other desktop platforms, please adapt accordingly!
</p>
<h2><span class="mw-headline" id="love.graphics.validateShader">love.graphics.validateShader</span></h2>
<p>Since <a href="/wiki/11.0" title="11.0">11.0</a>, there's <a href="/wiki/love.graphics.validateShader" title="love.graphics.validateShader">love.graphics.validateShader</a> which can be used to validate both GLSL and ESSL shader code. Note that it only validates, it doesn't run your shader code. If you really want to run your game under GLES context, proceed below!
</p>
<h2><span class="mw-headline" id="Requirements">Requirements</span></h2>
<p>You need GPU which supports OpenGL ES context or ANGLE. As previously written, todays desktop GPU should support this, thus ANGLE is normally unnecessary.
</p>
<h2><span class="mw-headline" id="Tell_LOVE_to_use_OpenGL_ES_context">Tell LOVE to use OpenGL ES context</span></h2>
<p>Now, we need to tell LOVE to use OpenGL ES context. You need to set this environment variable before starting LOVE. It can be set using Command Prompt/Terminal and launching LOVE from same Terminal, or from Windows, from Advanced System Settings.
</p>
<ul><li> Windows: <code>set LOVE_GRAPHICS_USE_OPENGLES=1</code> then <code>lovec ...</code>. <a href="/wiki/0.10.2" title="0.10.2">0.10.2</a> or later is assumed.</li></ul>
<ul><li> Linux/macOS: <code>LOVE_GRAPHICS_USE_OPENGLES=1 love ...</code></li></ul>
<p>Verify if LOVE uses OpenGL ES context by checking the 1st return value of <a href="/wiki/love.graphics.getRendererInfo" title="love.graphics.getRendererInfo">love.graphics.getRendererInfo</a> which should be "OpenGL ES".
</p><p>Once you got OpenGL ES context, you can test your shader code if it works under mobile devices. Otherwise, if you're on Windows, proceed below.
</p>
<h2><span class="mw-headline" id="Using_ANGLE_in_Windows">Using ANGLE in Windows</span></h2>
<p><a rel="nofollow" class="external text" href="https://chromium.googlesource.com/angle/angle">ANGLE</a> is an implementation of OpenGL ES 2.0 and 3.0 on top of other rendering backends. In Windows, it will run OpenGL ES using Direct3D 11 or Direct3D 9. If your GPU doesn't support OpenGL ES context or if you want to force ANGLE, you can copy <b>libEGL.dll</b> and <b>libGLESv2.dll</b> from one of these application:
</p>
<ul><li> Google Chrome</li></ul>
<ul><li> Microsoft Edge Chromium</li></ul>
<ul><li> Visual Studio Code</li></ul>
<ul><li> osu! stable (32-bit DLL)</li></ul>
<p>Protip: If you logged in to GitHub, you can grab weekly compiled binaries as artifact at <a rel="nofollow" class="external text" href="https://github.com/MikuAuahDark/angle-winbuild">here</a>.
</p><p>Note: Using Firefox's DLLs doesn't work because it links with Firefox-specific DLLs and copying all the dependent DLLs can be troublesome.
</p><p>One thing for sure is <b>make sure the bitness of the DLL matches</b>, i.e. 64-bit Chrome has 64-bit DLLs and can only be used on 64-bit LOVE.
</p><p>Next, set the environment <code>LOVE_GRAPHICS_USE_OPENGLES=1</code> and <code>SDL_OPENGL_ES_DRIVER=1</code> then launch LOVE. To confirm you're using ANGLE backend, check the 2nd return value of <a href="/wiki/love.graphics.getRendererInfo" title="love.graphics.getRendererInfo">love.graphics.getRendererInfo</a> which should contain "ANGLE" (all uppercase).
</p><p><br />
</p>
<!-- 
NewPP limit report
CPU time usage: 0.016 seconds
Real time usage: 0.018 seconds
Preprocessor visited node count: 16/1000000
Preprocessor generated node count: 28/1000000
Post‐expand include size: 35/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key love2d_wiki:pcache:idhash:6275-0!*!0!!*!*!* and timestamp 20201201163837 and revision id 25539
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;oldid=25539">https://love2d.org/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;oldid=25539</a>"</div>
					<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Tutorials" title="Category:Tutorials">Tutorials</a></li></ul></div></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=GLES+Shader+Testing+in+Desktop" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://github.com/love2d/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/GLES_Shader_Testing_in_Desktop" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/GLES_Shader_Testing_in_Desktop" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;oldid=25539" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=GLES_Shader_Testing_in_Desktop&amp;action=info">Page information</a></li>
											<li id="t-smwbrowselink"><a href="/wiki/Special:Browse/GLES_Shader_Testing_in_Desktop" rel="smw-browse">Browse properties</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="https://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 17 November 2020, at 08:55.</li>
									<li id="viewcount">This page has been accessed 5,098 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="https://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.tooltips","mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":89});
}</script></body></html>
