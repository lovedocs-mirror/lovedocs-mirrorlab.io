<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Getting Started (Français) - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="Getting Started (Français)" href="/w/index.php?title=Special:ExportRDF/Getting_Started_(Fran%C3%A7ais)&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Getting_Started_(Fran%C3%A7ais)" />
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=ext.geshi.language.lua%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Getting_Started_(Français)","wgTitle":"Getting Started (Français)","wgCurRevisionId":3225,"wgRevisionId":3225,"wgArticleId":728,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["LÖVE"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Getting_Started_(Français)","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.style","mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Getting_Started_Français skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-main" class="selected"><a href="/wiki/Getting_Started_(Fran%C3%A7ais)" title="View the content page [c]" accesskey="c">Page</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=Talk:Getting_Started_(Fran%C3%A7ais)&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=Getting_Started_(Fran%C3%A7ais)&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=Getting_Started_(Fran%C3%A7ais)&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">Getting Started (Français)</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><h2><span class="mw-headline" id="Obtenir_L.C3.96VE">Obtenir LÖVE</span></h2>
<p>Téléchargez la dernière version de LÖVE depuis <a rel="nofollow" class="external text" href="http://love2d.org/#download">le site officiel</a>, puis installez-la. Si vous êtes sous Windows et que vous ne voulez pas <i>installer</i> LÖVE, vous pouvez aussi simplement télécharger les exécutables au format zip et les extraire n' importe où.
</p><p>Vous pouvez vérifier votre version de Löve en tapant cette commande:
</p><p><code>
love --version
</code>
</p>
<h2><span class="mw-headline" id="Faire_un_jeu">Faire un jeu</span></h2>
<p>Pour faire un jeu minime, créez un dossier où vous le souhaitez, et ouvrez votre éditeur de code favori. Notepad++ est un bon éditeur pour Windows et possède même un support natif du langage Lua. Créez un nouveau fichier dans votre nouveau dossier, et nommez-le <i>main.lua</i>. Insérez le code suivant dans celui-ci et sauvegardez-le.
</p>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>draw<span class="br0">&#40;</span><span class="br0">&#41;</span>
    love<span class="sy0">.</span>graphics<span class="sy0">.</span><span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Hello World&quot;</span><span class="sy0">,</span> <span class="nu0">400</span><span class="sy0">,</span> <span class="nu0">300</span><span class="br0">&#41;</span>
<span class="kw1">end</span></pre></div></div>
<h2><span class="mw-headline" id="Lancer_des_jeux">Lancer des jeux</span></h2>
<p>Il y a deux manières de lancer un jeu dans LÖVE:
</p>
<ul><li> Depuis un dossier. </li>
<li> Depuis un fichier <i>.love</i> (un fichier <i>.zip</i> renommé).</li></ul>
<p>Dans les deux cas, un fichier <i>main.lua</i> doit être présent à la racine du dossier concerné. Ce fichier sera chargé lorsque LÖVE démarrera. Si ce fichier est manquant, LÖVE ne reconnaitra pas le dossier ou le fichier <i>.love</i> comme un jeu, et va se plaindre qu'il s'agit d'un jeu mal organisé.
</p><p>Une erreur fréquemment commise est de zipper le fichier plutôt que le contenu. Cela vient d'une très vieille habitude (parce que lorsque vous dézippez un dossier vous ne voulez pas étalez tout le contenu sur votre répertoire courant), mais pour LÖVE, faire ceci n'a pas de sens: vous devez zipper seulement le contenu du dossier du jeu pour obtenir un .love correct.
</p>
<h3><span class="mw-headline" id="Windows">Windows</span></h3>
<p>Sur Windows, la manière la plus facile pour lancer un jeu est de glisser le dossier sur le fichier <i>love.exe</i>. Assurez-vous bien que vous déplacer le dossier et non le fichier <i>main.lua</i> seul.
Il existe aussi l'option <a href="/wiki/Scite" title="Scite" class="mw-redirect">Scite</a>.
</p><p>Vous pouvez aussi lancer un jeu à partir d'une ligne de commande:
</p><p>Par exemple:
</p>
<pre>
love C:\games\mygame
love C:\games\packagedgame.love
</pre>
<p>Sur Windows, il existe une option spéciale qui permet d'attacher la console à la fenêtre de Löve. Cela peut vous permettre de voir la sortie standard.
</p><p><code>.
love --console
</code>
</p>
<h3><span class="mw-headline" id="Linux">Linux</span></h3>
<p>Sur Linux, vous pouvez utiliser ces lignes de commandes:
</p>
<pre>
love /home/path/to/game
love /home/path/to/packagedgame.love
</pre>
<p>Si vous avez installé le fichier <i>.deb</i>, vous pouvez bien-sûr double-cliquer sur les fichiers <i>.love</i> dans le gestionnaire de fichiers.
</p>
<h3><span class="mw-headline" id="Mac_OSX">Mac OSX</span></h3>
<p>Sur Mac OSX, un dossier ou un fichier .love peut être glissé sur le paquet d'application Love.app. Sur le Terminal Mac OSX (la ligne de commande ), vous pouvez utiliser Löve de cette façon (en supposant qu' il est déjà installé dans le répertoire Applications):
</p><p><code>open -a love mygame</code>
</p><p>Dans certains cas, il peut être plus rapide d'invoquer le binaire love à l'intérieur du lot d'application directement via ce qui suit:
</p><p><code>/Applications/love.app/Contents/MacOS/love mygame</code>
</p><p>Vous pouvez configurer un alias dans votre session Terminal pour nommer le binaire quand vous utilisez <code>love</code> en ajoutant un alias vers votre ~/.bash_profile (<code>open -a TextEdit ~/.bash_profile</code>):
</p>
<pre>
# alias to love
alias love=&quot;/Applications/love.app/Contents/MacOS/love&quot;
</pre>
<p>Maintenant vous pouvez faire appel à la ligne de commande comme sur Linux et Windows:
</p><p><code>
love /home/path/to/game
</code>
</p>
<h2><span class="mw-headline" id="Etapes_suivantes">Etapes suivantes</span></h2>
<ul><li> <a href="/wiki/Tutoriel:Fonctions_de_retours_d%27appels" title="Tutoriel:Fonctions de retours d'appels" class="mw-redirect">Tutoriel:Fonctions de retours d'appels</a> va vous enseigner la structure de base d'un jeu Löve.</li>
<li> <a href="/wiki/Categorie:Tutoriels" title="Categorie:Tutoriels">Categorie:Tutoriels</a> sont les autres et prochains tutoriels.</li></ul>
<h2><span class="mw-headline" id="Autres_langues">Autres langues</span></h2>
<div class="i18n">
<p><a href="/wiki/Getting_Started_(Dansk)" title="Getting Started (Dansk)">Dansk</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Deutsch)" title="Getting Started (Deutsch)">Deutsch</a>&#160;&#8211;
<a href="/wiki/Getting_Started" title="Getting Started">English</a>&#160;&#8211; 
<a href="/wiki/Getting_Started_(Espa%C3%B1ol)" title="Getting Started (Español)">Español</a>&#160;&#8211;
<strong class="selflink">Français</strong>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(Indonesia)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Indonesia) (page does not exist)">Indonesia</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Italiano)" title="Getting Started (Italiano)">Italiano</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(Lietuvi%C5%A1kai)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Lietuviškai) (page does not exist)">Lietuviškai</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Magyar)" title="Getting Started (Magyar)">Magyar</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Nederlands)" title="Getting Started (Nederlands)">Nederlands</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Polski)" title="Getting Started (Polski)">Polski</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Portugu%C3%AAs)" title="Getting Started (Português)">Português</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Rom%C3%A2n%C4%83)" title="Getting Started (Română)">Română</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(Slovensk%C3%BD)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Slovenský) (page does not exist)">Slovenský</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(Suomi)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Suomi) (page does not exist)">Suomi</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Svenska)" title="Getting Started (Svenska)">Svenska</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(T%C3%BCrk%C3%A7e)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Türkçe) (page does not exist)">Türkçe</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%C4%8Cesky)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Česky) (page does not exist)">Česky</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%CE%95%CE%BB%CE%BB%CE%B7%CE%BD%CE%B9%CE%BA%CE%AC)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Ελληνικά) (page does not exist)">Ελληνικά</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Български) (page does not exist)">Български</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9)" title="Getting Started (Русский)">Русский</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%D0%A1%D1%80%D0%BF%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Српски) (page does not exist)">Српски</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D1%81%D1%8C%D0%BA%D0%B0)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (Українська) (page does not exist)">Українська</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%D7%A2%D7%91%D7%A8%D7%99%D7%AA)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (עברית) (page does not exist)">עברית</a>&#160;&#8211;
<a href="/w/index.php?title=Getting_Started_(%E0%B9%84%E0%B8%97%E0%B8%A2)&amp;action=edit&amp;redlink=1" class="new" title="Getting Started (ไทย) (page does not exist)">ไทย</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(%E6%97%A5%E6%9C%AC%E8%AA%9E)" title="Getting Started (日本語)">日本語</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)" title="Getting Started (正體中文)">正體中文</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)" title="Getting Started (简体中文)">简体中文</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(Ti%E1%BA%BFng_Vi%E1%BB%87t)" title="Getting Started (Tiếng Việt)">Tiếng Việt</a>&#160;&#8211;
<a href="/wiki/Getting_Started_(%ED%95%9C%EA%B5%AD%EC%96%B4)" title="Getting Started (한국어)">한국어</a> 
<br />
<span style="text-align:right;"><i><a href="/wiki/Help:i18n" title="Help:i18n">More info</a></i></span>
</p>
</div>

<!-- 
NewPP limit report
CPU time usage: 0.052 seconds
Real time usage: 0.058 seconds
Preprocessor visited node count: 298/1000000
Preprocessor generated node count: 691/1000000
Post‐expand include size: 3347/2097152 bytes
Template argument size: 1430/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key love2d_wiki:pcache:idhash:728-0!*!0!!*!*!* and timestamp 20180619044718 and revision id 3225
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=Getting_Started_(Français)&amp;oldid=3225">https://love2d.org/w/index.php?title=Getting_Started_(Français)&amp;oldid=3225</a>"</div>
					<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:L%C3%96VE" title="Category:LÖVE">LÖVE</a></li></ul></div></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=Getting+Started+%28Fran%C3%A7ais%29" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://bitbucket.org/rude/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Getting_Started_(Fran%C3%A7ais)" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Getting_Started_(Fran%C3%A7ais)" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=Getting_Started_(Fran%C3%A7ais)&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=Getting_Started_(Fran%C3%A7ais)&amp;oldid=3225" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=Getting_Started_(Fran%C3%A7ais)&amp;action=info">Page information</a></li>
											<li id="t-smwbrowselink"><a href="/wiki/Special:Browse/Getting_Started_(Fran%C3%A7ais)" rel="smw-browse">Browse properties</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="http://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 25 October 2010, at 22:42.</li>
									<li id="viewcount">This page has been accessed 15,892 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.tooltips","mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":98});
}</script></body></html>
