<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>MediaWiki:Love.js - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="MediaWiki:Love.js" href="/w/index.php?title=Special:ExportRDF/MediaWiki:Love.js&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/MediaWiki:Love.js" />
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=ext.geshi.language.javascript%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"MediaWiki","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":8,"wgPageName":"MediaWiki:Love.js","wgTitle":"Love.js","wgCurRevisionId":23735,"wgRevisionId":23735,"wgArticleId":4242,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"javascript","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"MediaWiki:Love.js","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.smw.style"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-8 ns-subject page-MediaWiki_Love_js skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-mediawiki" class="selected"><a href="/wiki/MediaWiki:Love.js" title="View the system message [c]" accesskey="c">Message</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=MediaWiki_talk:Love.js&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=MediaWiki:Love.js&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=MediaWiki:Love.js&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">MediaWiki:Love.js</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div id="mw-clearyourcache" lang="en" dir="ltr" class="mw-content-ltr">
<p><strong>Note:</strong> After saving, you may have to bypass your browser's cache to see the changes.
</p>
<ul><li> <strong>Firefox / Safari:</strong> Hold <em>Shift</em> while clicking <em>Reload</em>, or press either <em>Ctrl-F5</em> or <em>Ctrl-R</em> (<em>⌘-R</em> on a Mac)</li>
<li> <strong>Google Chrome:</strong> Press <em>Ctrl-Shift-R</em> (<em>⌘-Shift-R</em> on a Mac)</li>
<li> <strong>Internet Explorer:</strong> Hold <em>Ctrl</em> while clicking <em>Refresh</em>, or press <em>Ctrl-F5</em></li>
<li> <strong>Opera:</strong> Clear the cache in <em>Tools → Preferences</em></li></ul>
</div>
<div dir="ltr"><pre class="javascript source-javascript"><span class="co1">// Source on github: https://github.com/airstruck/love-wiki-version-picker</span>
<span class="kw1">function</span> versionpicker<span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
	<span class="kw1">var</span> picker <span class="sy0">=</span> document.<span class="me1">createElement</span><span class="br0">&#40;</span><span class="st0">'select'</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
	picker.<span class="me1">onchange</span> <span class="sy0">=</span> <span class="kw1">function</span> <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		applyFilter<span class="br0">&#40;</span><span class="kw1">this</span>.<span class="me1">options</span><span class="br0">&#91;</span><span class="kw1">this</span>.<span class="me1">selectedIndex</span><span class="br0">&#93;</span>.<span class="me1">value</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> array <span class="br0">&#40;</span>a<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">return</span> <span class="br0">&#91;</span><span class="br0">&#93;</span>.<span class="me1">slice</span>.<span class="me1">apply</span><span class="br0">&#40;</span>a <span class="sy0">||</span> <span class="br0">&#91;</span><span class="br0">&#93;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="co1">// Some helpers for localStorage, basically resort to doing nothing</span>
	<span class="co1">// if it isn't available.</span>
	<span class="kw1">function</span> storeValue<span class="br0">&#40;</span>name<span class="sy0">,</span> value<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">try</span> <span class="br0">&#123;</span>
			localStorage<span class="br0">&#91;</span>name<span class="br0">&#93;</span> <span class="sy0">=</span> value<span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">catch</span> <span class="br0">&#40;</span>e<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> retrieveValue<span class="br0">&#40;</span>name<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">try</span> <span class="br0">&#123;</span>
			<span class="kw1">return</span> localStorage<span class="br0">&#91;</span>name<span class="br0">&#93;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">catch</span> <span class="br0">&#40;</span>e<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">return</span> <span class="kw2">undefined</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">var</span> pickerVersions <span class="sy0">=</span> <span class="br0">&#123;</span><span class="br0">&#125;</span><span class="sy0">;</span>
&nbsp;
	<span class="kw1">function</span> injectPickerOption <span class="br0">&#40;</span>version<span class="sy0">,</span> text<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>pickerVersions<span class="br0">&#91;</span>version<span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">return</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		pickerVersions<span class="br0">&#91;</span>version<span class="br0">&#93;</span> <span class="sy0">=</span> <span class="kw2">true</span><span class="sy0">;</span>
&nbsp;
		<span class="kw1">var</span> option <span class="sy0">=</span> document.<span class="me1">createElement</span><span class="br0">&#40;</span><span class="st0">'option'</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
		option.<span class="me1">value</span> <span class="sy0">=</span> version<span class="sy0">;</span>
		option.<span class="me1">textContent</span> <span class="sy0">=</span> text<span class="sy0">;</span>
		picker.<span class="me1">appendChild</span><span class="br0">&#40;</span>option<span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> makeWrapper <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> wrapper <span class="sy0">=</span> document.<span class="me1">createElement</span><span class="br0">&#40;</span><span class="st0">'div'</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
		wrapper.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-filterable'</span><span class="sy0">,</span> <span class="kw2">true</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
		<span class="kw1">return</span> wrapper<span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> wrapSections <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> element <span class="sy0">=</span> document.<span class="me1">querySelector</span><span class="br0">&#40;</span><span class="st0">'h2'</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="kw1">var</span> wrapper<span class="sy0">;</span>
&nbsp;
		while <span class="br0">&#40;</span>element<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> next <span class="sy0">=</span> element.<span class="me1">nextSibling</span><span class="sy0">;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span>element.<span class="me1">nodeName</span> <span class="sy0">==</span> <span class="st0">'H2'</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				wrapper <span class="sy0">=</span> makeWrapper<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
				element.<span class="me1">parentNode</span>.<span class="me1">insertBefore</span><span class="br0">&#40;</span>wrapper<span class="sy0">,</span> element<span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
			wrapper.<span class="me1">appendChild</span><span class="br0">&#40;</span>element<span class="br0">&#41;</span><span class="sy0">;</span>
			element <span class="sy0">=</span> next<span class="sy0">;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> updateSectionFromNote <span class="br0">&#40;</span>section<span class="sy0">,</span> note<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>note.<span class="me1">textContent</span>.<span class="me1">match</span><span class="br0">&#40;</span><span class="co2">/available since/i</span><span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> a <span class="sy0">=</span> note.<span class="me1">querySelector</span><span class="br0">&#40;</span><span class="st0">'a'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			section.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-added'</span><span class="sy0">,</span> a.<span class="me1">title</span><span class="br0">&#41;</span><span class="sy0">;</span>
			note.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-note'</span><span class="sy0">,</span> <span class="kw2">true</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>note.<span class="me1">textContent</span>.<span class="me1">match</span><span class="br0">&#40;</span><span class="co2">/removed in/i</span><span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> a <span class="sy0">=</span> note.<span class="me1">querySelector</span><span class="br0">&#40;</span><span class="st0">'a'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			section.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-removed'</span><span class="sy0">,</span> a.<span class="me1">title</span><span class="br0">&#41;</span><span class="sy0">;</span>
			note.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-note'</span><span class="sy0">,</span> <span class="kw2">true</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> QueryIterator <span class="br0">&#40;</span>query<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">return</span> <span class="kw1">function</span> <span class="br0">&#40;</span>callback<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">return</span> array<span class="br0">&#40;</span>document.<span class="me1">querySelectorAll</span><span class="br0">&#40;</span>query<span class="br0">&#41;</span><span class="br0">&#41;</span>.<span class="me1">forEach</span><span class="br0">&#40;</span>callback<span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">var</span> withSections <span class="sy0">=</span> QueryIterator<span class="br0">&#40;</span><span class="st0">'*[data-love-filterable]'</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="kw1">var</span> withAdded <span class="sy0">=</span> QueryIterator<span class="br0">&#40;</span><span class="st0">'*[data-love-version-added]'</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="kw1">var</span> withRemoved <span class="sy0">=</span> QueryIterator<span class="br0">&#40;</span><span class="st0">'*[data-love-version-removed]'</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="kw1">var</span> withAddedRemoved <span class="sy0">=</span> QueryIterator<span class="br0">&#40;</span><span class="st0">'*[data-love-version-added][data-love-version-removed]'</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="kw1">var</span> withNotes <span class="sy0">=</span> QueryIterator<span class="br0">&#40;</span><span class="st0">'*[data-love-version-note]'</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
	<span class="kw1">function</span> hideNotes <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		withNotes<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>note<span class="br0">&#41;</span> <span class="br0">&#123;</span> note.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="st0">'none'</span> <span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> showNotes <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		withNotes<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>note<span class="br0">&#41;</span> <span class="br0">&#123;</span> note.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="kw2">null</span> <span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">var</span> needsFilter <span class="sy0">=</span> <span class="kw2">false</span><span class="sy0">;</span>
&nbsp;
	<span class="kw1">function</span> scanSections <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		withSections<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> note <span class="sy0">=</span> section.<span class="me1">querySelector</span><span class="br0">&#40;</span><span class="st0">'table'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span><span class="sy0">!</span>note<span class="br0">&#41;</span> <span class="br0">&#123;</span>
				<span class="kw1">return</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
			updateSectionFromNote<span class="br0">&#40;</span>section<span class="sy0">,</span> note<span class="br0">&#41;</span><span class="sy0">;</span>
			while <span class="br0">&#40;</span>note.<span class="me1">nextSibling</span> <span class="sy0">&amp;&amp;</span> note.<span class="me1">nextSibling</span>.<span class="me1">tagName</span> <span class="sy0">==</span> <span class="st0">'TABLE'</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				note <span class="sy0">=</span> note.<span class="me1">nextSibling</span><span class="sy0">;</span>
				updateSectionFromNote<span class="br0">&#40;</span>section<span class="sy0">,</span> note<span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
			needsFilter <span class="sy0">=</span> <span class="kw2">true</span><span class="sy0">;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> scanTables <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> added <span class="sy0">=</span> document.<span class="me1">querySelectorAll</span><span class="br0">&#40;</span><span class="st0">'.smwtable *[alt=&quot;Added since&quot;] + *'</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="kw1">var</span> removed <span class="sy0">=</span> document.<span class="me1">querySelectorAll</span><span class="br0">&#40;</span><span class="st0">'.smwtable *[alt=&quot;Removed in&quot;] + *'</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
		array<span class="br0">&#40;</span>added<span class="br0">&#41;</span>.<span class="me1">forEach</span><span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>a<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> e <span class="sy0">=</span> a.<span class="me1">parentNode</span>.<span class="me1">parentNode</span><span class="sy0">;</span>
			e.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-filterable'</span><span class="sy0">,</span> <span class="kw2">true</span><span class="br0">&#41;</span><span class="sy0">;</span>
			e.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-added'</span><span class="sy0">,</span> a.<span class="me1">title</span><span class="br0">&#41;</span><span class="sy0">;</span>
			needsFilter <span class="sy0">=</span> <span class="kw2">true</span><span class="sy0">;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		array<span class="br0">&#40;</span>removed<span class="br0">&#41;</span>.<span class="me1">forEach</span><span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>a<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> e <span class="sy0">=</span> a.<span class="me1">parentNode</span>.<span class="me1">parentNode</span><span class="sy0">;</span>
			e.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-filterable'</span><span class="sy0">,</span> <span class="kw2">true</span><span class="br0">&#41;</span><span class="sy0">;</span>
			e.<span class="me1">setAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-removed'</span><span class="sy0">,</span> a.<span class="me1">title</span><span class="br0">&#41;</span><span class="sy0">;</span>
			needsFilter <span class="sy0">=</span> <span class="kw2">true</span><span class="sy0">;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="co1">// return true if 'installed' is greater than or equal to 'required'</span>
	<span class="co1">// http://stackoverflow.com/a/6832670</span>
	<span class="kw1">function</span> compareVersions <span class="br0">&#40;</span>installed<span class="sy0">,</span> required<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> a <span class="sy0">=</span> installed.<span class="me1">split</span><span class="br0">&#40;</span><span class="st0">'.'</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="kw1">var</span> b <span class="sy0">=</span> required.<span class="me1">split</span><span class="br0">&#40;</span><span class="st0">'.'</span><span class="br0">&#41;</span><span class="sy0">;</span>
&nbsp;
		<span class="kw1">for</span> <span class="br0">&#40;</span><span class="kw1">var</span> i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> a.<span class="me1">length</span><span class="sy0">;</span> <span class="sy0">++</span>i<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			a<span class="br0">&#91;</span>i<span class="br0">&#93;</span> <span class="sy0">=</span> <span class="kw4">Number</span><span class="br0">&#40;</span>a<span class="br0">&#91;</span>i<span class="br0">&#93;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">for</span> <span class="br0">&#40;</span><span class="kw1">var</span> i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> b.<span class="me1">length</span><span class="sy0">;</span> <span class="sy0">++</span>i<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			b<span class="br0">&#91;</span>i<span class="br0">&#93;</span> <span class="sy0">=</span> <span class="kw4">Number</span><span class="br0">&#40;</span>b<span class="br0">&#91;</span>i<span class="br0">&#93;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>a.<span class="me1">length</span> <span class="sy0">==</span> <span class="nu0">2</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			a<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span> <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>b.<span class="me1">length</span> <span class="sy0">==</span> <span class="nu0">2</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			b<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span> <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
&nbsp;
		<span class="kw1">if</span> <span class="br0">&#40;</span>a<span class="br0">&#91;</span><span class="nu0">0</span><span class="br0">&#93;</span> <span class="sy0">&gt;</span> b<span class="br0">&#91;</span><span class="nu0">0</span><span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="kw1">return</span> <span class="kw2">true</span><span class="sy0">;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>a<span class="br0">&#91;</span><span class="nu0">0</span><span class="br0">&#93;</span> <span class="sy0">&lt;</span> b<span class="br0">&#91;</span><span class="nu0">0</span><span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="kw1">return</span> <span class="kw2">false</span><span class="sy0">;</span>
&nbsp;
		<span class="kw1">if</span> <span class="br0">&#40;</span>a<span class="br0">&#91;</span><span class="nu0">1</span><span class="br0">&#93;</span> <span class="sy0">&gt;</span> b<span class="br0">&#91;</span><span class="nu0">1</span><span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="kw1">return</span> <span class="kw2">true</span><span class="sy0">;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>a<span class="br0">&#91;</span><span class="nu0">1</span><span class="br0">&#93;</span> <span class="sy0">&lt;</span> b<span class="br0">&#91;</span><span class="nu0">1</span><span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="kw1">return</span> <span class="kw2">false</span><span class="sy0">;</span>
&nbsp;
		<span class="kw1">if</span> <span class="br0">&#40;</span>a<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span> <span class="sy0">&gt;</span> b<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="kw1">return</span> <span class="kw2">true</span><span class="sy0">;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>a<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span> <span class="sy0">&lt;</span> b<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span><span class="br0">&#41;</span> <span class="kw1">return</span> <span class="kw2">false</span><span class="sy0">;</span>
&nbsp;
		<span class="kw1">return</span> <span class="kw2">true</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> filterVersion <span class="br0">&#40;</span>installed<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		filterAll<span class="br0">&#40;</span>hideNotes<span class="br0">&#41;</span><span class="sy0">;</span>
		withAdded<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> required <span class="sy0">=</span> section.<span class="me1">getAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-added'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span><span class="sy0">!</span>compareVersions<span class="br0">&#40;</span>installed<span class="sy0">,</span> required<span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				section.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="st0">'none'</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		withRemoved<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> removed <span class="sy0">=</span> section.<span class="me1">getAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-removed'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span>compareVersions <span class="br0">&#40;</span>installed<span class="sy0">,</span> removed<span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				section.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="st0">'none'</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		withAddedRemoved<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> added <span class="sy0">=</span> section.<span class="me1">getAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-added'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">var</span> removed <span class="sy0">=</span> section.<span class="me1">getAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-removed'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span>compareVersions<span class="br0">&#40;</span>added<span class="sy0">,</span> removed<span class="br0">&#41;</span> <span class="sy0">&amp;&amp;</span> compareVersions<span class="br0">&#40;</span>installed<span class="sy0">,</span> added<span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				section.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="kw2">null</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> filterAll <span class="br0">&#40;</span>hideOrShowNotes<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		hideOrShowNotes<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		withSections<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span> section.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="kw2">null</span> <span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> filterLatest <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		filterAll<span class="br0">&#40;</span>hideNotes<span class="br0">&#41;</span><span class="sy0">;</span>
		withRemoved<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span> section.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="st0">'none'</span> <span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		withAddedRemoved<span class="br0">&#40;</span><span class="kw1">function</span> <span class="br0">&#40;</span>section<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> added <span class="sy0">=</span> section.<span class="me1">getAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-added'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">var</span> removed <span class="sy0">=</span> section.<span class="me1">getAttribute</span><span class="br0">&#40;</span><span class="st0">'data-love-version-removed'</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span>compareVersions<span class="br0">&#40;</span>added<span class="sy0">,</span> removed<span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				section.<span class="me1">style</span>.<span class="me1">display</span> <span class="sy0">=</span> <span class="kw2">null</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
		<span class="br0">&#125;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> applyFilter <span class="br0">&#40;</span>value<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		storeValue<span class="br0">&#40;</span><span class="st0">'versionpicker-version'</span><span class="sy0">,</span> value<span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>value <span class="sy0">==</span> <span class="st0">'all'</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			filterAll<span class="br0">&#40;</span>showNotes<span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">return</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>value <span class="sy0">==</span> <span class="st0">'latest'</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			filterLatest<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="kw1">return</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
		filterVersion<span class="br0">&#40;</span>value<span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> injectFilterPicker <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> target <span class="sy0">=</span> document.<span class="me1">getElementById</span><span class="br0">&#40;</span><span class="st0">'ca-nstab-main'</span><span class="br0">&#41;</span>.<span class="me1">parentNode</span><span class="sy0">;</span>
&nbsp;
		picker.<span class="me1">style</span>.<span class="me1">position</span> <span class="sy0">=</span> <span class="st0">'absolute'</span><span class="sy0">;</span>
		picker.<span class="me1">style</span>.<span class="me1">right</span> <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span>
		picker.<span class="me1">style</span>.<span class="me1">height</span> <span class="sy0">=</span> target.<span class="me1">parentNode</span>.<span class="me1">offsetHeight</span> <span class="sy0">-</span> <span class="nu0">8</span> <span class="sy0">+</span> <span class="st0">'px'</span><span class="sy0">;</span>
		picker.<span class="me1">style</span>.<span class="me1">margin</span> <span class="sy0">=</span> <span class="st0">'4px'</span><span class="sy0">;</span>
&nbsp;
		target.<span class="me1">parentNode</span>.<span class="me1">insertBefore</span><span class="br0">&#40;</span>picker<span class="sy0">,</span> target<span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> injectVersionTag <span class="br0">&#40;</span>number<span class="sy0">,</span> name<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		injectPickerOption<span class="br0">&#40;</span>number<span class="sy0">,</span> <span class="st0">'Version '</span> <span class="sy0">+</span> number <span class="sy0">+</span> <span class="st0">': '</span> <span class="sy0">+</span> name<span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> injectVersionTags <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> versions <span class="sy0">=</span> <span class="br0">&#91;</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;11.3&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Mysterious Mysteries&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;11.2&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Mysterious Mysteries&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;11.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Mysterious Mysteries&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;11.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Mysterious Mysteries&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.10.2&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Super Toast&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.10.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Super Toast&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.10.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Super Toast&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.9.2&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Baby Inspector&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.9.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Baby Inspector&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.9.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Baby Inspector&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.8.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Rubber Piggy&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.7.2&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Game Slave&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.7.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Game Slave&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.7.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Game Slave&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.6.2&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Jiggly Juice&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.6.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Jiggly Juice&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.6.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Jiggly Juice&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.5.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Salted Nuts&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.4.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Taco Beam&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.3.2&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Lemony Fresh&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.3.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Space Meat&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.3.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Mutant Vermin&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.2.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Impending Doom&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.2.0&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Mini Moose&quot;</span><span class="br0">&#125;</span><span class="sy0">,</span>
			<span class="br0">&#123;</span><span class="st0">&quot;version&quot;</span><span class="sy0">:</span> <span class="st0">&quot;0.1.1&quot;</span><span class="sy0">,</span> <span class="st0">&quot;codename&quot;</span><span class="sy0">:</span> <span class="st0">&quot;Santa Power&quot;</span><span class="br0">&#125;</span>
		<span class="br0">&#93;</span>
&nbsp;
		<span class="kw1">for</span> <span class="br0">&#40;</span><span class="kw1">var</span> i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> versions.<span class="me1">length</span><span class="sy0">;</span> i<span class="sy0">++</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			injectVersionTag<span class="br0">&#40;</span>versions<span class="br0">&#91;</span>i<span class="br0">&#93;</span>.<span class="me1">version</span><span class="sy0">,</span> versions<span class="br0">&#91;</span>i<span class="br0">&#93;</span>.<span class="me1">codename</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> queryVersionTags <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">var</span> url <span class="sy0">=</span> <span class="st0">'/w/index.php?title=Version_History&amp;action=raw'</span><span class="sy0">;</span>
		<span class="kw1">var</span> xhr <span class="sy0">=</span> <span class="kw1">new</span> XMLHttpRequest<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		xhr.<span class="me1">onload</span> <span class="sy0">=</span> <span class="kw1">function</span> <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">var</span> re <span class="sy0">=</span> <span class="co2">/\[\[([\d.]+)\]\][^\n]*\n[^\|]*\|(.*)\n/gm</span><span class="sy0">;</span>
			<span class="kw1">var</span> m<span class="sy0">;</span>
&nbsp;
			while <span class="br0">&#40;</span>m <span class="sy0">=</span> re.<span class="me1">exec</span><span class="br0">&#40;</span><span class="kw1">this</span>.<span class="me1">responseText</span><span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
				injectVersionTag<span class="br0">&#40;</span>m<span class="br0">&#91;</span><span class="nu0">1</span><span class="br0">&#93;</span><span class="sy0">,</span> m<span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span><span class="br0">&#41;</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
		<span class="br0">&#125;</span>
		xhr.<span class="me1">open</span><span class="br0">&#40;</span><span class="st0">'get'</span><span class="sy0">,</span> url<span class="sy0">,</span> <span class="kw2">true</span><span class="br0">&#41;</span><span class="sy0">;</span>
		xhr.<span class="me1">send</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> setPicker <span class="br0">&#40;</span>target<span class="br0">&#41;</span> <span class="br0">&#123;</span>
		<span class="kw1">for</span> <span class="br0">&#40;</span><span class="kw1">var</span> i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> picker.<span class="me1">options</span>.<span class="me1">length</span><span class="sy0">;</span> <span class="sy0">++</span>i<span class="br0">&#41;</span> <span class="br0">&#123;</span>
			<span class="kw1">if</span> <span class="br0">&#40;</span>picker.<span class="me1">options</span><span class="br0">&#91;</span>i<span class="br0">&#93;</span>.<span class="me1">value</span> <span class="sy0">===</span> target<span class="br0">&#41;</span> <span class="br0">&#123;</span>
				picker.<span class="me1">selectedIndex</span> <span class="sy0">=</span> i<span class="sy0">;</span>
				picker.<span class="me1">onchange</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
				<span class="kw1">break</span><span class="sy0">;</span>
			<span class="br0">&#125;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	<span class="kw1">function</span> main <span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
		wrapSections<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		scanSections<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		scanTables<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		injectPickerOption<span class="br0">&#40;</span><span class="st0">'all'</span><span class="sy0">,</span> <span class="st0">'All Versions'</span><span class="br0">&#41;</span><span class="sy0">;</span>
		injectPickerOption<span class="br0">&#40;</span><span class="st0">'latest'</span><span class="sy0">,</span> <span class="st0">'Latest Version'</span><span class="br0">&#41;</span><span class="sy0">;</span>
		injectVersionTags<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		injectFilterPicker<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="kw1">var</span> target <span class="sy0">=</span> retrieveValue<span class="br0">&#40;</span><span class="st0">'versionpicker-version'</span><span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="kw1">if</span> <span class="br0">&#40;</span>target <span class="sy0">!==</span> <span class="kw2">undefined</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
			setPicker<span class="br0">&#40;</span>target<span class="br0">&#41;</span><span class="sy0">;</span>
		<span class="br0">&#125;</span>
	<span class="br0">&#125;</span>
&nbsp;
	main<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="br0">&#125;</span>
&nbsp;
versionpicker<span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy0">;</span></pre></div></div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=MediaWiki:Love.js&amp;oldid=23735">https://love2d.org/w/index.php?title=MediaWiki:Love.js&amp;oldid=23735</a>"</div>
					<div id='catlinks' class='catlinks catlinks-allhidden'></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=MediaWiki%3ALove.js" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://bitbucket.org/rude/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/MediaWiki:Love.js" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/MediaWiki:Love.js" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=MediaWiki:Love.js&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=MediaWiki:Love.js&amp;oldid=23735" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=MediaWiki:Love.js&amp;action=info">Page information</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="http://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 27 October 2019, at 16:35.</li>
									<li id="viewcount">This page has been accessed 13,224 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":156});
}</script></body></html>
