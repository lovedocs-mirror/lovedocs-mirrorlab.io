<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Comparing LÖVE Versions - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="Comparing LÖVE Versions" href="/w/index.php?title=Special:ExportRDF/Comparing_L%C3%96VE_Versions&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Comparing_L%C3%96VE_Versions" />
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=ext.geshi.language.lua%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Comparing_LÖVE_Versions","wgTitle":"Comparing LÖVE Versions","wgCurRevisionId":24623,"wgRevisionId":24623,"wgArticleId":6563,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Snippets"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Comparing_LÖVE_Versions","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.style","mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Comparing_LÖVE_Versions skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-main" class="selected"><a href="/wiki/Comparing_L%C3%96VE_Versions" title="View the content page [c]" accesskey="c">Page</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=Talk:Comparing_L%C3%96VE_Versions&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=Comparing_L%C3%96VE_Versions&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=Comparing_L%C3%96VE_Versions&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">Comparing LÖVE Versions</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><p>It's sometimes useful to check for certain LÖVE versions, for example, do automatic color conversion to 0..1 range in version <a href="/wiki/11.0" title="11.0">11.0</a> and later, and keep using 0..255 range in <a href="/wiki/0.10.2" title="0.10.2">0.10.2</a> and earlier.
</p>
<h2><span class="mw-headline" id="Function">Function</span></h2>
<h3><span class="mw-headline" id="Synopsis">Synopsis</span></h3>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1">compare <span class="sy0">=</span> compareLOVEVersion<span class="br0">&#40;</span>major<span class="sy0">,</span> minor<span class="sy0">,</span> revision<span class="br0">&#41;</span></pre></div></div>
<h3><span class="mw-headline" id="Arguments">Arguments</span></h3>
<dl><dt><code><a href="/wiki/number" title="number">number</a> major</code></dt>
<dd> Major version</dd>
<dt><code><a href="/wiki/number" title="number">number</a> minor (nil)</code></dt>
<dd> Minor version</dd>
<dt><code><a href="/wiki/number" title="number">number</a> revision (nil)</code></dt>
<dd> Revision</dd></dl>
<h3><span class="mw-headline" id="Returns">Returns</span></h3>
<dl><dt><code><a href="/wiki/number" title="number">number</a> compare</code></dt>
<dd> -1 if current LÖVE version is lower than specified, 0 if <i>exactly</i> equal, 1 if it's later version</dd></dl>
<h3><span class="mw-headline" id="Examples">Examples</span></h3>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="co1">-- LÖVE version: 0.9.2</span>
compareLOVEVersion<span class="br0">&#40;</span><span class="nu0">0</span><span class="sy0">,</span> <span class="nu0">9</span><span class="sy0">,</span> <span class="nu0">2</span><span class="br0">&#41;</span> <span class="co1">-- returns 0</span>
&#160;
<span class="co1">-- LÖVE version: 0.10.0</span>
compareLOVEVersion<span class="br0">&#40;</span><span class="nu0">11</span><span class="sy0">,</span> <span class="nu0">1</span><span class="br0">&#41;</span> <span class="co1">-- returns -1</span>
&#160;
<span class="co1">-- LÖVE version: 0.10.2</span>
compareLOVEVersion<span class="br0">&#40;</span><span class="nu0">0</span><span class="sy0">,</span> <span class="nu0">10</span><span class="sy0">,</span> <span class="nu0">0</span><span class="br0">&#41;</span> <span class="co1">-- returns 1</span></pre></div></div>
<h3><span class="mw-headline" id="Source">Source</span></h3>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> compareLOVEVersion<span class="br0">&#40;</span>maj<span class="sy0">,</span> <span class="kw3">min</span><span class="sy0">,</span> rev<span class="br0">&#41;</span>
	<span class="kw1">if</span> love<span class="sy0">.</span>_version_major <span class="sy0">&gt;</span> maj <span class="kw1">then</span>
		<span class="kw1">return</span> <span class="nu0">1</span>
	<span class="kw1">elseif</span> love<span class="sy0">.</span>_version_major <span class="sy0">&lt;</span> maj <span class="kw1">then</span>
		<span class="kw1">return</span> <span class="sy0">-</span><span class="nu0">1</span>
	<span class="kw1">elseif</span> <span class="kw3">min</span> <span class="kw1">then</span>
		<span class="kw1">if</span> love<span class="sy0">.</span>_version_minor <span class="sy0">&gt;</span> <span class="kw3">min</span> <span class="kw1">then</span>
			<span class="kw1">return</span> <span class="nu0">1</span>
		<span class="kw1">elseif</span> love<span class="sy0">.</span>_version_minor <span class="sy0">&lt;</span> <span class="kw3">min</span> <span class="kw1">then</span>
			<span class="kw1">return</span> <span class="sy0">-</span><span class="nu0">1</span>
		<span class="kw1">elseif</span> rev <span class="kw1">then</span>
			<span class="kw1">if</span> love<span class="sy0">.</span>_version_revision <span class="sy0">&gt;</span> rev <span class="kw1">then</span>
				<span class="kw1">return</span> <span class="nu0">1</span>
			<span class="kw1">elseif</span> love<span class="sy0">.</span>_version_revision <span class="sy0">&lt;</span> rev <span class="kw1">then</span>
				<span class="kw1">return</span> <span class="sy0">-</span><span class="nu0">1</span>
			<span class="kw1">end</span>
		<span class="kw1">end</span>
	<span class="kw1">end</span>
	<span class="co1">-- equal</span>
	<span class="kw1">return</span> <span class="nu0">0</span>
<span class="kw1">end</span></pre></div></div>
<p><br />
</p>
<h2><span class="mw-headline" id="Other_Languages">Other Languages</span></h2>
<div class="i18n">
<p><a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Dansk)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Dansk) (page does not exist)">Dansk</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Deutsch)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Deutsch) (page does not exist)">Deutsch</a>&#160;&#8211;
<strong class="selflink">English</strong>&#160;&#8211; 
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Espa%C3%B1ol)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Español) (page does not exist)">Español</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Fran%C3%A7ais)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Français) (page does not exist)">Français</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Indonesia)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Indonesia) (page does not exist)">Indonesia</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Italiano)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Italiano) (page does not exist)">Italiano</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Lietuvi%C5%A1kai)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Lietuviškai) (page does not exist)">Lietuviškai</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Magyar)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Magyar) (page does not exist)">Magyar</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Nederlands)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Nederlands) (page does not exist)">Nederlands</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Polski)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Polski) (page does not exist)">Polski</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Portugu%C3%AAs)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Português) (page does not exist)">Português</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Rom%C3%A2n%C4%83)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Română) (page does not exist)">Română</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Slovensk%C3%BD)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Slovenský) (page does not exist)">Slovenský</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Suomi)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Suomi) (page does not exist)">Suomi</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Svenska)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Svenska) (page does not exist)">Svenska</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(T%C3%BCrk%C3%A7e)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Türkçe) (page does not exist)">Türkçe</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%C4%8Cesky)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Česky) (page does not exist)">Česky</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%CE%95%CE%BB%CE%BB%CE%B7%CE%BD%CE%B9%CE%BA%CE%AC)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Ελληνικά) (page does not exist)">Ελληνικά</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Български) (page does not exist)">Български</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Русский) (page does not exist)">Русский</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%D0%A1%D1%80%D0%BF%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Српски) (page does not exist)">Српски</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D1%81%D1%8C%D0%BA%D0%B0)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Українська) (page does not exist)">Українська</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%D7%A2%D7%91%D7%A8%D7%99%D7%AA)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (עברית) (page does not exist)">עברית</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%E0%B9%84%E0%B8%97%E0%B8%A2)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (ไทย) (page does not exist)">ไทย</a>&#160;&#8211;
<a href="/wiki/Comparing_L%C3%96VE_Versions_(%E6%97%A5%E6%9C%AC%E8%AA%9E)" title="Comparing LÖVE Versions (日本語)">日本語</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (正體中文) (page does not exist)">正體中文</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (简体中文) (page does not exist)">简体中文</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(Ti%E1%BA%BFng_Vi%E1%BB%87t)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (Tiếng Việt) (page does not exist)">Tiếng Việt</a>&#160;&#8211;
<a href="/w/index.php?title=Comparing_L%C3%96VE_Versions_(%ED%95%9C%EA%B5%AD%EC%96%B4)&amp;action=edit&amp;redlink=1" class="new" title="Comparing LÖVE Versions (한국어) (page does not exist)">한국어</a> 
<br />
<span style="text-align:right;"><i><a href="/wiki/Help:i18n" title="Help:i18n">More info</a></i></span>
</p>
</div>

<!-- 
NewPP limit report
CPU time usage: 0.068 seconds
Real time usage: 0.069 seconds
Preprocessor visited node count: 344/1000000
Preprocessor generated node count: 909/1000000
Post‐expand include size: 4258/2097152 bytes
Template argument size: 2147/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key love2d_wiki:pcache:idhash:6563-0!*!0!!*!*!* and timestamp 20191225135320 and revision id 24623
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=Comparing_LÖVE_Versions&amp;oldid=24623">https://love2d.org/w/index.php?title=Comparing_LÖVE_Versions&amp;oldid=24623</a>"</div>
					<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Snippets" title="Category:Snippets">Snippets</a></li></ul></div></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=Comparing+L%C3%96VE+Versions" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://bitbucket.org/rude/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Comparing_L%C3%96VE_Versions" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Comparing_L%C3%96VE_Versions" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=Comparing_L%C3%96VE_Versions&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=Comparing_L%C3%96VE_Versions&amp;oldid=24623" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=Comparing_L%C3%96VE_Versions&amp;action=info">Page information</a></li>
											<li id="t-smwbrowselink"><a href="/wiki/Special:Browse/Comparing_L%C3%96VE_Versions" rel="smw-browse">Browse properties</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="http://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 17 December 2019, at 07:17.</li>
									<li id="viewcount">This page has been accessed 1,586 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["ext.smw.tooltips","mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":183});
}</script></body></html>
