<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Tutorial:Callback Functions (Deutsch) - LOVE</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="ExportRDF" type="application/rdf+xml" title="Tutorial:Callback Functions (Deutsch)" href="/w/index.php?title=Special:ExportRDF/Tutorial:Callback_Functions_(Deutsch)&amp;xmlmime=rdf" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="LOVE (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://love2d.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Tutorial:Callback_Functions_(Deutsch)" />
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html" />
<link rel="alternate" type="application/atom+xml" title="LOVE Atom feed" href="/w/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=ext.geshi.language.lua%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.love.styles&amp;only=styles&amp;skin=love&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/w/skins/Love/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/w/skins/Love/IE70Fixes.css?303" media="screen" /><![endif]--><meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=love&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: love2d_wiki:resourceloader:filter:minify-css:7:daf253d59690fd9cabb6b95510bce103 */</style>
<script src="https://love2d.org/w/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=love&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"Tutorial","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":500,"wgPageName":"Tutorial:Callback_Functions_(Deutsch)","wgTitle":"Callback Functions (Deutsch)","wgCurRevisionId":16505,"wgRevisionId":16505,"wgArticleId":1807,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorials (Deutsch)"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Callback_Functions_(Deutsch)","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"love","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs102":false,"searchNs103":false,"searchNs104":false,"searchNs105":false,"searchNs108":false,"searchNs109":false,"searchNs500":false,"searchNs501":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: love2d_wiki:resourceloader:filter:minify-js:7:201bb6cc0b4c032fe7bbe209a0125541 */
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.smw.style"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-500 ns-subject page-Tutorial_Callback_Functions_Deutsch skin-love action-view">
<div id="globalWrapper">
		<div id="column-content">
			<div id="content" class="mw-body" role="main">
				<a id="top"></a>
				
				
						<div id="p-cactions" role="navigation">
			<h5>Views</h5>

			<div>
				<ul>
				<li id="ca-nstab-tutorial" class="selected"><a href="/wiki/Tutorial:Callback_Functions_(Deutsch)">Tutorial</a></li>
				<li id="ca-talk" class="new"><a href="/w/index.php?title=Tutorial_talk:Callback_Functions_(Deutsch)&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
				<li id="ca-viewsource"><a href="/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
				<li id="ca-history"><a href="/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>
				</ul>
							</div>
		</div>
	
				<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Callback Functions (Deutsch)</h1>

				<div id="bodyContent">
					<!-- <div id="siteSub">From LOVE</div> -->

					<!-- <div id="contentSub"></div> -->
										<!-- <div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a></div> -->

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><p>Die <a href="/wiki/Category:Callbacks" title="Category:Callbacks">callback</a> Funktionen werden von <a href="/wiki/love.run" title="love.run">love.run</a> aufgerufen und führen unterschiedliche Aufgaben aus. Diese Aufgaben sind optional, jedoch würde ein umfangreiches Spiel mit hoher Wahrscheinlichkeit nahezu jede dieser <a href="/wiki/Category:Callbacks" title="Category:Callbacks">callback</a> Funktionen nutzen, wodurch es ratsam wäre zu Wissen was jede einzelne von ihnen bewirkt.
</p>
<h2><span class="mw-headline" id="love.load"><a href="/wiki/love.load" title="love.load">love.load</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>load<span class="br0">&#40;</span><span class="br0">&#41;</span>
   bild <span class="sy0">=</span> love<span class="sy0">.</span>graphics<span class="sy0">.</span>newImage<span class="br0">&#40;</span><span class="st0">&quot;kuchen.jpg&quot;</span><span class="br0">&#41;</span>
   <span class="kw1">local</span> f <span class="sy0">=</span> love<span class="sy0">.</span>graphics<span class="sy0">.</span>newFont<span class="br0">&#40;</span><span class="nu0">12</span><span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span>setFont<span class="br0">&#40;</span>f<span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span>setColor<span class="br0">&#40;</span><span class="nu0">0</span><span class="sy0">,</span><span class="nu0">0</span><span class="sy0">,</span><span class="nu0">0</span><span class="sy0">,</span><span class="nu0">255</span><span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span>setBackgroundColor<span class="br0">&#40;</span><span class="nu0">255</span><span class="sy0">,</span><span class="nu0">255</span><span class="sy0">,</span><span class="nu0">255</span><span class="br0">&#41;</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird nur ein einziges Mal beim Spielstart aufgerufen. Hier werden also normalerweise die Ressourcen geladen, Variablen initialisiert und spezielle Einstellungen getroffen. Dies kann auch woanders gemacht werden, wenn man es aber in dieser Funktion macht, wird es nur einmal ausgeführt, was einiges an Systemressourcen spart.
</p>
<h2><span class="mw-headline" id="love.update"><a href="/wiki/love.update" title="love.update">love.update</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>update<span class="br0">&#40;</span>dt<span class="br0">&#41;</span>
   <span class="kw1">if</span> love<span class="sy0">.</span>keyboard<span class="sy0">.</span>isDown<span class="br0">&#40;</span><span class="st0">&quot;up&quot;</span><span class="br0">&#41;</span> <span class="kw1">then</span>
      num <span class="sy0">=</span> num <span class="sy0">+</span> <span class="nu0">100</span> <span class="sy0">*</span> dt <span class="co1">--Dies erhöht num um 100 pro Sekunde</span>
   <span class="kw1">end</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird ununterbrochen aufgerufen und wird die Funktion sein, inder die meisten Berechnungen stattfinden. 'dt' steht für "<a href="/wiki/love.timer.getDelta" title="love.timer.getDelta">delta time</a>" und ist die Anzahl der Sekunden die seit dem letzten Funktionsaufruf vergangen sind (dies ist normalerweise ein sehr kleiner Wert z.B.: 0.025714).
</p>
<h2><span class="mw-headline" id="love.draw"><a href="/wiki/love.draw" title="love.draw">love.draw</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>draw<span class="br0">&#40;</span><span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span>draw<span class="br0">&#40;</span>bild<span class="sy0">,</span> bildx<span class="sy0">,</span> bildy<span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span><span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Klicke und ziehe den Kuchen herum oder benutze die Pfeiltasten.&quot;</span><span class="sy0">,</span> <span class="nu0">10</span><span class="sy0">,</span> <span class="nu0">10</span><span class="br0">&#41;</span>
<span class="kw1">end</span></pre></div></div>
<p>In <code><a href="/wiki/love.draw" title="love.draw">love.draw</a></code>  kommt alles hinein was mit dem Zeichen des Bildschirms zu tun hat (draw = zeichnen). Alle <code><a href="/wiki/love.graphics.draw" title="love.graphics.draw">love.graphics.draw</a></code> Funktionen müssen hier aufgerufen werden, außerhalb haben sie keine Wirkung (es ist aber möglich z.B.: das Zeichnen einer Karte in eine andere Funktion zu verlagern und diese Funktion dann hier aufzurufen). Auch diese Funktion wird ununterbrochen aufgerufen, deshalb ist zu beachten, dass wenn du den font/color/mode/usw. am Ende der Funktion änderst, dies Auswirkung auf die gesamte Funktion hat, also auch auf den Anfang. Zum Beispiel:
</p>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>load<span class="br0">&#40;</span><span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span>setColor<span class="br0">&#40;</span><span class="nu0">0</span><span class="sy0">,</span><span class="nu0">0</span><span class="sy0">,</span><span class="nu0">0</span><span class="br0">&#41;</span> <span class="co1">--Setzt die Farbe auf Schwarz</span>
<span class="kw1">end</span>
&#160;
<span class="kw1">function</span> love<span class="sy0">.</span>draw<span class="br0">&#40;</span><span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span><span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Dieser Text ist nicht!!! schwarz, wegen der Zeile darunter.&quot;</span><span class="sy0">,</span> <span class="nu0">100</span><span class="sy0">,</span> <span class="nu0">100</span><span class="br0">&#41;</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span>setColor<span class="br0">&#40;</span><span class="nu0">255</span><span class="sy0">,</span><span class="nu0">0</span><span class="sy0">,</span><span class="nu0">0</span><span class="br0">&#41;</span> <span class="co1">--Setzt die Farbe auf Rot</span>
   love<span class="sy0">.</span>graphics<span class="sy0">.</span><span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Dieser Text ist rot.&quot;</span><span class="sy0">,</span> <span class="nu0">100</span><span class="sy0">,</span> <span class="nu0">200</span><span class="br0">&#41;</span>
<span class="kw1">end</span></pre></div></div>
<h2><span class="mw-headline" id="love.mousepressed"><a href="/wiki/love.mousepressed" title="love.mousepressed">love.mousepressed</a></span></h2>
<table class="new-section" bgcolor="#d0d0ff" style="margin-top: 1em; padding: 3px; border-radius:5px; -moz-border-radius:5px;" data-newin="&#91;&#91;0.10.0]]">
<tr>
<td><i> <b>Available since LÖVE <a href="/wiki/0.10.0" title="0.10.0">0.10.0</a></b> </i>
</td></tr>
<tr>
<td> <small>This variant is not supported in earlier versions.</small>
</td></tr></table>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>mousepressed<span class="br0">&#40;</span>x<span class="sy0">,</span> y<span class="sy0">,</span> maustaste<span class="sy0">,</span> istouch<span class="br0">&#41;</span>
   <span class="kw1">if</span> maustaste <span class="sy0">==</span> <span class="nu0">1</span> <span class="kw1">then</span>
      bildx <span class="sy0">=</span> x <span class="co1">--Bewegt das Bild zu der Position im Fenster auf die geklickt wurde</span>
      bildy <span class="sy0">=</span> y
   <span class="kw1">end</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird immer bei einem Mausklick aufgerufen und bekommt die aktuelle Zeigerposition und die Taste die gedrückt wurde übergeben. Die Taste kann eines der <a href="/wiki/MouseConstant" title="MouseConstant">constants</a> sein. Die Funktion harmoniert gut mit <code><a href="/wiki/love.mousereleased" title="love.mousereleased">love.mousereleased</a></code>.
</p>
<h2><span class="mw-headline" id="love.mousereleased"><a href="/wiki/love.mousereleased" title="love.mousereleased">love.mousereleased</a></span></h2>
<table class="new-section" bgcolor="#d0d0ff" style="margin-top: 1em; padding: 3px; border-radius:5px; -moz-border-radius:5px;" data-newin="&#91;&#91;0.10.0]]">
<tr>
<td><i> <b>Available since LÖVE <a href="/wiki/0.10.0" title="0.10.0">0.10.0</a></b> </i>
</td></tr>
<tr>
<td> <small>This variant is not supported in earlier versions.</small>
</td></tr></table>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>mousereleased<span class="br0">&#40;</span>x<span class="sy0">,</span> y<span class="sy0">,</span> maustaste<span class="sy0">,</span> istouch<span class="br0">&#41;</span>
   <span class="kw1">if</span> maustaste <span class="sy0">==</span> <span class="nu0">1</span> <span class="kw1">then</span>
      kamehameha<span class="br0">&#40;</span>x<span class="sy0">,</span>y<span class="br0">&#41;</span> <span class="co1">--Diese total super geile, selbst erstellte Funktion, ist woanders definiert</span>
   <span class="kw1">end</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird immer beim Loslassen einer Maustaste aufgerufen und bekommt die aktuelle Zeigerposition und die Taste die losgelassen wurde übergeben. Du kannst diese Funktion zusammen mit <code><a href="/wiki/love.mousepressed" title="love.mousepressed">love.mousepressed</a></code> oder separat benutzen, sie sind in keinster Weise miteinander verbunden.
</p>
<h2><span class="mw-headline" id="love.keypressed"><a href="/wiki/love.keypressed" title="love.keypressed">love.keypressed</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>keypressed<span class="br0">&#40;</span>taste<span class="sy0">,</span> unicode<span class="br0">&#41;</span>
   <span class="kw1">if</span> taste <span class="sy0">==</span> <span class="st0">'b'</span> <span class="kw1">then</span>
      text <span class="sy0">=</span> <span class="st0">&quot;Die B-Taste wurde gedrückt.&quot;</span>
   <span class="kw1">elseif</span> taste <span class="sy0">==</span> <span class="st0">'a'</span> <span class="kw1">then</span>
      a_gedrueckt <span class="sy0">=</span> <span class="kw4">true</span>
   <span class="kw1">end</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird immer bei einem Tastendruck auf der Tastatur aufgerufen und bekommt die Taste die gedrückt wurde übergeben. Die Taste kann jede von den <a href="/wiki/KeyConstant_(Deutsch)" title="KeyConstant (Deutsch)">Konstanten</a> sein. Diese Funktion harmoniert gut mit <code><a href="/wiki/love.keyreleased" title="love.keyreleased">love.keyreleased</a></code>.
</p>
<h2><span class="mw-headline" id="love.keyreleased"><a href="/wiki/love.keyreleased" title="love.keyreleased">love.keyreleased</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>keyreleased<span class="br0">&#40;</span>taste<span class="sy0">,</span> unicode<span class="br0">&#41;</span>
   <span class="kw1">if</span> taste <span class="sy0">==</span> <span class="st0">'b'</span> <span class="kw1">then</span>
      text <span class="sy0">=</span> <span class="st0">&quot;Die B-Taste wurde losgelassen&quot;</span>
   <span class="kw1">elseif</span> key <span class="sy0">==</span> <span class="st0">'a'</span> <span class="kw1">then</span>
      a_gedrueckt <span class="sy0">=</span> <span class="kw4">false</span>
   <span class="kw1">end</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird immer beim Loslassen einer Taste auf der Tastatur aufgerufen und bekommt die losgelassene Taste übergeben. Du kannst diese Funktion zusammen mit <code><a href="/wiki/love.keypressed" title="love.keypressed">love.keypressed</a></code>  oder separat benutzen, sie sind in keinster Weise miteinander verbunden.
</p>
<h2><span class="mw-headline" id="love.focus"><a href="/wiki/love.focus" title="love.focus">love.focus</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>focus<span class="br0">&#40;</span>f<span class="br0">&#41;</span>
  <span class="kw1">if</span> <span class="kw2">not</span> f <span class="kw1">then</span>
    <span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Fokussierung des Fensters verloren&quot;</span><span class="br0">&#41;</span> <span class="co1">--Fenster abgewählt</span>
  <span class="kw1">else</span>
    <span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Fokussierung des Fensters erhalten&quot;</span><span class="br0">&#41;</span> <span class="co1">--Fenster angewählt</span>
  <span class="kw1">end</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird immer dann aufgerufen, wenn der Benutzer das LÖVE-Fenster an /- bzw. abwählt. Als Beispiel: Falls der Benutzer grade in einem Fenster spielt (nicht Vollbild)und währenddessen auf einen Ort außerhalb des Fensters klickt, kann das Spiel dies bemerken und sich automatisch pausieren.
</p>
<h2><span class="mw-headline" id="love.quit"><a href="/wiki/love.quit" title="love.quit">love.quit</a></span></h2>
<div dir="ltr" class="mw-geshi mw-code mw-content-ltr"><div class="lua source-lua"><pre class="de1"><span class="kw1">function</span> love<span class="sy0">.</span>quit<span class="br0">&#40;</span><span class="br0">&#41;</span>
  <span class="kw3">print</span><span class="br0">&#40;</span><span class="st0">&quot;Danke fürs Spielen! Komm bald wieder.&#160;:D&quot;</span><span class="br0">&#41;</span> <span class="co1">--Wird in der Konsole ausgegeben</span>
<span class="kw1">end</span></pre></div></div>
<p>Diese Funktion wird immer aufgerufen, wenn der Benutzer das Programm beenden will (meistens durch das X am Fenster). Hier könnte das Spiel dann automatisch gespeichert werden.
</p><p>Das sind die 'Callback' Funktionen und ihre grundlegende Benutzung.
</p><p><br />
</p><p><br />
</p>
<h2><span class="mw-headline" id="Andere_Sprachen">Andere Sprachen</span></h2>
<div class="i18n">
<p><a href="/w/index.php?title=Tutorial:Callback_Functions_(Dansk)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Dansk) (page does not exist)">Dansk</a>&#160;&#8211;
<strong class="selflink">Deutsch</strong>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions" title="Tutorial:Callback Functions">English</a>&#160;&#8211; 
<a href="/wiki/Tutorial:Callback_Functions_(Espa%C3%B1ol)" title="Tutorial:Callback Functions (Español)">Español</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(Fran%C3%A7ais)" title="Tutorial:Callback Functions (Français)">Français</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Indonesia)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Indonesia) (page does not exist)">Indonesia</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(Italiano)" title="Tutorial:Callback Functions (Italiano)">Italiano</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Lietuvi%C5%A1kai)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Lietuviškai) (page does not exist)">Lietuviškai</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Magyar)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Magyar) (page does not exist)">Magyar</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Nederlands)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Nederlands) (page does not exist)">Nederlands</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(Polski)" title="Tutorial:Callback Functions (Polski)">Polski</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(Portugu%C3%AAs)" title="Tutorial:Callback Functions (Português)">Português</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Rom%C3%A2n%C4%83)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Română) (page does not exist)">Română</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Slovensk%C3%BD)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Slovenský) (page does not exist)">Slovenský</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(Suomi)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Suomi) (page does not exist)">Suomi</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(Svenska)" title="Tutorial:Callback Functions (Svenska)">Svenska</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(T%C3%BCrk%C3%A7e)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Türkçe) (page does not exist)">Türkçe</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%C4%8Cesky)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Česky) (page does not exist)">Česky</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%CE%95%CE%BB%CE%BB%CE%B7%CE%BD%CE%B9%CE%BA%CE%AC)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Ελληνικά) (page does not exist)">Ελληνικά</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Български) (page does not exist)">Български</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9)" title="Tutorial:Callback Functions (Русский)">Русский</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%D0%A1%D1%80%D0%BF%D1%81%D0%BA%D0%B8)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Српски) (page does not exist)">Српски</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D1%81%D1%8C%D0%BA%D0%B0)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (Українська) (page does not exist)">Українська</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%D7%A2%D7%91%D7%A8%D7%99%D7%AA)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (עברית) (page does not exist)">עברית</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%E0%B9%84%E0%B8%97%E0%B8%A2)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (ไทย) (page does not exist)">ไทย</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(%E6%97%A5%E6%9C%AC%E8%AA%9E)" title="Tutorial:Callback Functions (日本語)">日本語</a>&#160;&#8211;
<a href="/w/index.php?title=Tutorial:Callback_Functions_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)&amp;action=edit&amp;redlink=1" class="new" title="Tutorial:Callback Functions (正體中文) (page does not exist)">正體中文</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)" title="Tutorial:Callback Functions (简体中文)">简体中文</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(Ti%E1%BA%BFng_Vi%E1%BB%87t)" title="Tutorial:Callback Functions (Tiếng Việt)">Tiếng Việt</a>&#160;&#8211;
<a href="/wiki/Tutorial:Callback_Functions_(%ED%95%9C%EA%B5%AD%EC%96%B4)" title="Tutorial:Callback Functions (한국어)">한국어</a> 
<br />
<span style="text-align:right;"><i><a href="/wiki/Help:i18n" title="Help:i18n">More info</a></i></span>
</p>
</div>

<!-- 
NewPP limit report
CPU time usage: 0.112 seconds
Real time usage: 0.119 seconds
Preprocessor visited node count: 431/1000000
Preprocessor generated node count: 1127/1000000
Post‐expand include size: 4781/2097152 bytes
Template argument size: 2208/2097152 bytes
Highest expansion depth: 5/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key love2d_wiki:pcache:idhash:1807-0!*!0!!*!*!* and timestamp 20180619030116 and revision id 16505
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://love2d.org/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;oldid=16505">https://love2d.org/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;oldid=16505</a>"</div>
					<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Tutorials_(Deutsch)" title="Category:Tutorials (Deutsch)">Tutorials (Deutsch)</a></li></ul></div></div>					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div id="column-one">
			<div class="portlet" id="p-personal" role="navigation">
				<h5>Personal tools</h5>

				<div class="pBody">
					<ul>
													<li id="pt-login"><a href="/w/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ACallback+Functions+%28Deutsch%29" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>
											</ul>
				</div>
			</div>
			<div class="portlet" id="p-logo" role="banner">
				<a href="/wiki/Main_Page" class="mw-wiki-logo" title="Visit the main page"></a>
			</div>
				<div class="generated-sidebar portlet" id="p-documentation" role="navigation">
		<h5>documentation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-love"><a href="/wiki/love">love</a></li>
											<li id="n-love.audio"><a href="/wiki/love.audio">love.audio</a></li>
											<li id="n-love.data"><a href="/wiki/love.data">love.data</a></li>
											<li id="n-love.event"><a href="/wiki/love.event">love.event</a></li>
											<li id="n-love.filesystem"><a href="/wiki/love.filesystem">love.filesystem</a></li>
											<li id="n-love.font"><a href="/wiki/love.font">love.font</a></li>
											<li id="n-love.graphics"><a href="/wiki/love.graphics">love.graphics</a></li>
											<li id="n-love.image"><a href="/wiki/love.image">love.image</a></li>
											<li id="n-love.joystick"><a href="/wiki/love.joystick">love.joystick</a></li>
											<li id="n-love.keyboard"><a href="/wiki/love.keyboard">love.keyboard</a></li>
											<li id="n-love.math"><a href="/wiki/love.math">love.math</a></li>
											<li id="n-love.mouse"><a href="/wiki/love.mouse">love.mouse</a></li>
											<li id="n-love.physics"><a href="/wiki/love.physics">love.physics</a></li>
											<li id="n-love.sound"><a href="/wiki/love.sound">love.sound</a></li>
											<li id="n-love.system"><a href="/wiki/love.system">love.system</a></li>
											<li id="n-love.thread"><a href="/wiki/love.thread">love.thread</a></li>
											<li id="n-love.timer"><a href="/wiki/love.timer">love.timer</a></li>
											<li id="n-love.touch"><a href="/wiki/love.touch">love.touch</a></li>
											<li id="n-love.video"><a href="/wiki/love.video">love.video</a></li>
											<li id="n-love.window"><a href="/wiki/love.window">love.window</a></li>
											<li id="n-lua-enet"><a href="/wiki/lua-enet">lua-enet</a></li>
											<li id="n-luasocket"><a href="/wiki/socket">luasocket</a></li>
											<li id="n-utf8"><a href="/wiki/utf8">utf8</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-navigation" role="navigation">
		<h5>Navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-Home"><a href="https://love2d.org/" rel="nofollow">Home</a></li>
											<li id="n-Forums"><a href="https://love2d.org/forums/" rel="nofollow">Forums</a></li>
											<li id="n-Issue-tracker"><a href="https://bitbucket.org/rude/love/issues" rel="nofollow">Issue tracker</a></li>
									</ul>
					</div>
		</div>
		<div class="generated-sidebar portlet" id="p-wiki_navigation" role="navigation">
		<h5>wiki navigation</h5>
		<div class='pBody'>
							<ul>
											<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
											<li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
											<li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li>
									</ul>
					</div>
		</div>
			<div id="p-search" class="portlet" role="search">
			<h5><label for="searchInput">Search</label></h5>

			<div id="searchBody" class="pBody">
				<form action="/w/index.php" id="searchform">
					<input type='hidden' name="title" value="Special:Search"/>
					<input type="search" name="search" placeholder="Search" title="Search LOVE [f]" accesskey="f" id="searchInput" />
					<input type="submit" name="go" value="Go" title="Go to a page with this exact name if exists" id="searchGoButton" class="searchButton" />&#160;
						<input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton" />
				</form>

							</div>
		</div>
			<div class="portlet" id="p-tb" role="navigation">
			<h5>Tools</h5>

			<div class="pBody">
				<ul>
											<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Callback_Functions_(Deutsch)" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>
											<li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Callback_Functions_(Deutsch)" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>
											<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>
											<li id="t-print"><a href="/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>
											<li id="t-permalink"><a href="/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;oldid=16505" title="Permanent link to this revision of the page">Permanent link</a></li>
											<li id="t-info"><a href="/w/index.php?title=Tutorial:Callback_Functions_(Deutsch)&amp;action=info">Page information</a></li>
									</ul>
							</div>
		</div>
			</div><!-- end of the left (by default at least) column -->
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo">
						<div id="f-copyrightico">
									<a href="http://www.gnu.org/copyleft/fdl.html"><img src="/w/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3" width="88" height="31" /></a>
							</div>
					<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/w/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
									<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/w/extensions/SemanticMediaWiki/includes/../resources/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 6 June 2016, at 23:23.</li>
									<li id="viewcount">This page has been accessed 11,495 times.</li>
									<li id="copyright">Content is available under <a class="external" rel="nofollow" href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a> unless otherwise noted.</li>
									<li id="privacy"><a href="/wiki/LOVE:Privacy_policy" title="LOVE:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/LOVE:About" title="LOVE:About">About LOVE</a></li>
									<li id="disclaimer"><a href="/wiki/LOVE:General_disclaimer" title="LOVE:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"loading","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"],null,true);
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://love2d.org/w/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=love\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":87});
}</script></body></html>
