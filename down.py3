#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright 2018 Pedro Gimeno Fortea
u"""Download LÖVE pages given the list in a file (parameter)."""

import sys
import os
import requests
import urllib.parse
# urllib.parse.quote_plus(str) or urllib.parse.quote(str, safe='')
from html.parser import HTMLParser
import tinycss2
import re
import io


def debug(*x):
    sys.stdout.write('\t'.join(x) + '\n')

baseURL = 'https://love2d.org/wiki/%s'
baseSaveLoc = 'raw/%s'
pending = []
visited = set()

proxies = {
    'http': 'http://127.0.0.1:8118',
    'https': 'http://127.0.0.1:8118'
}

match_html_in_script = re.compile(
    r'\bdocument\s*\.\s*write\s*\(\s*('
    r'"(?:\\[\S\s]|[^"])*"'
    r'|'
    r"'(?:\\[\S\s]|[^'])*'"
    r')\s*\)'
)

# Regex to find JS escapes in string
split_js_string = re.compile(
    r'\\\r\n|\\[bfnrtv"' "'\r\n\u2028\u2029" r'\\]|\\[0-3]?[0-7]{1,2}'
    r'|\\x[a-fA-F0-9]{2}|\\u[a-fA-F0-9]{4}|\\u\{[a-fA-F0-9]+\}'
    r'|\\[^xu]|([\\\r\n\u2028\u2029])|[^\\\\]+'
)

parse_url_head = re.compile('^(?:https?:)?//(?:www\.)?([-.a-z0-9]*)(?:$|/)',
    re.I)

parse_filemap = re.compile('^([^ ]*) (.*)$')


class Obj(object):

    """Storage object."""

    pass


def canonicalize(url, expect_foreign=False):
    """Returns a canonical version of a URL.

    Resolves .. and http:// etc.
    """
    if url.startswith('data:'):
        return url
    match = parse_url_head.search(url)
    if match:
        if match.group(1).lower() != 'love2d.org':
            if expect_foreign:
                return url
            assert False, "Foreign URL?!: " + url
        url = '/' + url[match.end(0):]
    assert url, "Empty URL in canonicalize()"
    if url[0] != '/':
        # URLs are relative to /wiki/
        url = '/wiki/' + url

    while '/./' in url:
        pos = url.index('/./')
        url = url[:pos] + url[pos + 2:]
    if url.endswith('/.'):
        url = url[:-1]

    while '/../' in url:
        pos = url.index('/../')
        part1 = url[:url.rfind('/', 0, pos) + 1]
        if not part1:
            part1 = ''
        url = part1 + url[pos + 4:]

    if url.endswith('/..'):
        url = url[:-3]
        if url:
            url = url[:url.rfind('/') + 1]
        else:
            url = '/'

    if url == '/wiki/':
        url = '/wiki/Main_Page'

    return url


def parse_js_string(s):
    """Parse a JS-style string's contents.

    Requires the contents of the string, without quotes.

    If it contains invalid Unicode or illegal escapes, we return None,
    as we can't process it. Otherwise we return the string value.
    """
    js_conv = {'b':'\b', 'f':'\f', 'n':'\n', 'r':'\r', 't':'\t', 'v':'\v'}
    bs = io.BytesIO()

    for match in split_js_string.finditer(s):
        if match.group(1):
            # group 1 is the error indicator group.
            return None
        ch = match.group(0)
        assert ch, "Empty match. This should not happen."
        if ch[0] != '\\':
            bs.write(ch.encode('utf-16-be'))
        elif ch[1] in ('\u2028\u2029\r\n'):
            pass  # output nothing as it's a line continuation
        elif ch[1] in js_conv:
            bs.write(js_conv[ch[1]].encode('utf-16-be'))
        elif ch[1] == 'x':
            bs.write(chr(int(ch[2:], 16)).encode('utf-16-be'))
        elif ch[1] == 'u':
            if ch[2] == '{':
                bs.write(chr(int(ch[3:-1], 16)).encode('utf-16-be'))
            else:
                bs.write(bytes.fromhex(ch[2:]))
        else:
            bs.write(ch[1].encode('utf-16-be'))

    value = bs.getvalue()
    try:
        value = value.decode('utf-16-be')
    except UnicodeDecodeError:
        pass

    return value


def scrape_js(script):
    for v in match_html_in_script.finditer(script):
        html = parse_js_string(v.group(1)[1:-1])
        # Mutually recurse to scrape the URLs inside the document.write code
        if html is not bytes:
            scrape_html(html)


def check_css_url_recursive(tree):
    """Recursively search through an AST node looking for URLs."""
    if not tree: return
    for rule in tree:
        if isinstance(rule, tinycss2.ast.QualifiedRule):
            check_css_url_recursive(rule.prelude)
            check_css_url_recursive(rule.content)
        elif isinstance(rule, tinycss2.ast.CurlyBracketsBlock):
            check_css_url_recursive(rule.content)
        elif isinstance(rule, tinycss2.ast.URLToken):
            # All URLs are expected to be images.
            if not rule.value.startswith('data:'):
                pending.append(('img', rule.value))
        elif isinstance(rule, tinycss2.ast.AtRule):
            if rule.lower_at_keyword == 'import':
                # This changes things. The immediate child must be a URL.
                # That URL is a CSS, not an image.
                for index, value in enumerate(rule.prelude):
                    if (isinstance(value, tinycss2.ast.StringToken)
                            or isinstance(value, tinycss2.ast.URLToken)):
                        pending.append(('css', value.value))
                        check_css_url_recursive(rule.prelude[index+1:])
                        break
            else:
                check_css_url_recursive(rule.prelude)
            check_css_url_recursive(rule.content)


def scrape_css(css):
    parse = tinycss2.parse_stylesheet(css)
    check_css_url_recursive(parse)


class ScrapeParser(HTMLParser):
    """HTMLParser to scrape URLs from the page."""

    def __init__(self, basedir='/wiki/'):
        """Constructor to init our members."""
        self.last_tag = None
        self.basedir = basedir
        super().__init__()

    def handle_starttag(self, tag, attrs):
        """Required handler."""
        self.last_tag = tag
        att = dict(attrs)
        if tag == 'a':
            if 'href' in att:
                # We are not interested in anything other than wiki articles,
                # and we already have the whole articles list. We don't need
                # this.
                # #pending.append(('page', att['href']))
                # But we want the full images in File:whatever.png.
                href = canonicalize(att['href'], expect_foreign=True)
                if (href.startswith('/w/')
                        and not href.startswith('/w/index.php?')):
                    pending.append(('img', att['href']))
            return
        if tag == 'link':
            if att.get('rel').lower() == 'stylesheet' and 'href' in att:
                pending.append(('css', att['href']))
            if att.get('rel').lower() == 'shortcut icon' and 'href' in att:
                pending.append(('img', att['href']))
            return
        if tag == 'script':
            if 'src' in att:
                pending.append(('js', att['src']))
            return
        if tag == 'img':
            pending.append(('img', att.get('src')))
            return

    def handle_endtag(self, tag):
        """Required handler."""
        self.last_tag = None

    def handle_startendtag(self, tag, attrs):
        """Required handler."""
        self.handle_starttag(tag, attrs)
        self.last_tag = None

    def handle_data(self, data):
        """Required handler."""
        if self.last_tag == 'script':
            scrape_js(data)
        if self.last_tag == 'style':
            scrape_css(data)


def save_file(saveloc, text):
    """Save a bytes object to the given file."""
    f = open(saveloc, 'wb')
    try:
        f.write(text)
    finally:
        f.close()


def load_file(loc):
    """Read a file returning it as a string."""
    f = open(loc, 'r')
    try:
        txt = f.read()
    finally:
        f.close()

    return txt


def scrape_html(body, kind='html'):
    """Scan for URLs in an HTML body."""
    scrape = ScrapeParser()
    scrape.feed(body)
    scrape.close()


def save_item(savename, contents):
    assert savename[0:1] == '/' and '\0' not in savename
    assert savename[1:2] != '/'
    if savename.startswith('/wiki/'):
        # Slashes in paths within /wiki/ are not directories; convert them
        # to %2F and % to %25.
        savename = '/wiki/' + savename[6:].replace(' ', '_').replace('%',
            '%25').replace('/', '%2F')
    savename = savename[1:]
    if '/' in savename:
        savename = baseSaveLoc % savename
        split = savename.rfind('/')
        dir = savename[0:split+1]
        if not os.path.exists(dir.encode('utf8')):
            os.makedirs(dir.encode('utf8'))
    else:
        savename = baseSaveLoc % savename
    basename = os.path.basename(savename)
    if len(basename) > 255 or basename == '.filemap.dir':
        # Oops! Need to convert that.
        # First, read the filemap just in case it already exists.
        filemap = {}
        encbn = urllib.parse.quote(basename.encode('utf8'), safe='')
        path = savename[:-len(basename)]
        try:
            f = open(path + '.filemap.dir', 'r')
            try:
                for line in f.readlines():
                    match = parse_filemap.search(line)
                    if match:
                        filemap[match.group(2)] = match.group(1)
            finally:
                f.close()
        except FileNotFoundError:
            pass
        if encbn in filemap:
            newname = filemap[encbn]
        else:
            import uuid
            newname = uuid.uuid4().hex
            f = open(path + '.filemap.dir', 'ab')
            try:
                f.write(newname.encode('utf8') + b' ' + encbn.encode('utf8')
                    + b'\n')
            finally:
                f.close()
            debug('save_item: Saving', savename, 'as', path + newname)
        savename = path + newname

    save_file(savename, contents)

def save_page(name):
    """Save a page by MediaWiki name."""
    #name = name.replace(' ', '_').replace('%', '%25').replace('/', '%2F')
    if canonicalize('/wiki/' + name) in visited:
        return

    assert name[0:1] != '/' and '\0' not in name

    debug('retrieving:', name)
    r = requests.get(baseURL % name, proxies=proxies)
    #r = Obj(); r.content = load_file(name); r.status_code = 200; r.url = 'https://love2d.org/wiki/' + name

    assert r.status_code == 200

    debug('saving page:', '/wiki/' + name)
    save_item('/wiki/' + name, r.content)
    scrape_html(r.text)

    #from pprint import pformat
    #debug(pformat(pending))

    visited.add(canonicalize('/wiki/' + name))

    # Process pending
    while pending:
        kind, item = pending.pop(0)
        item = canonicalize(item)
        #debug("Visiting pending:", item, kind)
        if item not in visited and not item.startswith('data:'):
            # Retrieve it
            visited.add(item)
            r = requests.get('https://love2d.org' + item, proxies=proxies)
            if kind == 'img':
                # Raw save
                debug('saving image:', item)
                save_item(item, r.content)
            elif kind == 'css':
                # Save and scrape
                debug('saving css:', item)
                save_item(item, r.content)
                scrape_css(r.text)
            elif kind == 'html':
                debug('saving html:', item)
                save_item(item, r.content)
                scrape_html(r.text)
            elif kind == 'js':
                debug('saving js:', item)
                save_item(item, r.content)
                scrape_js(item)
            else:
                assert False, "Invalid scrape kind: " + kind


def main():
    """Main."""
    assert len(sys.argv) > 1, "Argument required: list to load"

    pagelist = load_file(sys.argv[1])
    print(pagelist)
    for element in re.finditer('[^\n]+', pagelist):
        save_file(sys.argv[1] + '.last', element.group(0).encode('utf8'))

        save_page(element.group(0))

    if element:
        os.unlink(sys.argv[1] + '.last')


try:
    result = main()
except KeyboardInterrupt:
    result = 1

if result:
    sys.exit(result)
