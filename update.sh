#!/bin/bash
set -e
# Ensure we're in the master branch.
test "$(git symbolic-ref -q HEAD)" = refs/heads/master || { echo "Must be in master branch" ; exit 1; }
# If the index is not clean, we would commit changes that we haven't added.
# Rather than resetting the index, take the safest approach and abort without
# doing anything.
git diff-index --quiet --cached HEAD -- || { echo "Git index is not clean, aborting" ; exit 1 ; }
# Ensure there are no changes in the raw/ subdir, which is the one we commit
git diff --quiet raw cooked || { echo "There are changed files in the raw/ subdirectory; aborting" ; exit 1 ; }

echo Updating from: $(cat recent.ts)
python3 listrecent.py3 nextrecent $(cat recent.ts)
test -s nextrecent.lst || exit 0
python3 down.py3 nextrecent.lst
mv -f nextrecent.ts recent.ts
mv -f nextrecent.lst recent.lst
git add raw recent.lst recent.ts
git commit -m "Update to $(cat recent.ts)"
