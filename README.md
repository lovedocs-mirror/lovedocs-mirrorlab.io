# Unofficial mirror of the LÖVE wiki

This is an unofficial mirror of the love2d (LÖVE) wiki, updated automatically through a cron script in the [owner](https://gitlab.com/pgimeno)'s computer.

The Python scripts are used for downloading the wiki. `down.py` requires [tinycss2](https://github.com/Kozea/tinycss2) which in turn requires [webencodings](https://github.com/gsnedders/python-webencodings). Note that webencodings is available as a Debian package for current Debian stable, but tinycss2 is not.

There's no license decided for the code yet, so please don't use it until that is dealt with.
