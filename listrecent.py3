#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright 2018 Pedro Gimeno Fortea
"""Generate list of RECENT pages for use in down.py."""

import requests
import sys
import datetime


proxies = {
    'http': 'http://127.0.0.1:8118',
    'https': 'http://127.0.0.1:8118'
}

excludeNS = frozenset(('Talk', 'User talk', 'LOVE talk', 'File talk',
                       'Template talk', 'Help talk', 'Category talk',
                       'Property talk', 'Type talk', 'Concept talk',
                       'Tutorial talk', 'MediaWiki talk', 'Media', 'Special'))

nsURL = 'https://www.love2d.org/w/api.php?action=query' \
        '&meta=siteinfo&siprop=namespaces&format=json'

allURL = 'https://love2d.org/w/api.php?action=query&list=allpages' \
         '&aplimit=200&format=json'


recentURL = 'https://love2d.org/w/api.php?action=query&list=recentchanges' \
            '&rcend=%s&rcprop=title%%7Ctimestamp&rclimit=100&format=json'

pageList = []

Max = None

def save_file(saveloc, text):
    """Save a string at saveloc."""

    f = open(saveloc, 'w')
    try:
        f.write(text)
    finally:
        f.close()


def load_file(loc):
    """Read a file returning it as a string."""
    f = open(loc, 'r')
    try:
        txt = f.read()
    finally:
        f.close()

    return txt


def get_recent(ts):
    global Max
    url = recentURL % ts

    req = requests.get(url, proxies=proxies)
    while req.status_code == 200:
        pages = req.json()
        for k in pages['query']['recentchanges']:
            for ex in excludeNS:
                if (not k['title'].startswith(ex + ':')
                        and ('missing' not in k or not k['missing'])
                        and ('invalid' not in k or not k['invalid'])):
                    pageList.append(k['title'])
                    if not Max or Max < k['timestamp']:
                       Max = k['timestamp']

        break


def main():
    global Max
    assert len(sys.argv) > 2, "Two arguments needed: basename datetime" \
        " (ISO8601 accepted, see" \
        " https://www.mediawiki.org/w/api.php" \
        "?action=help&modules=main#main/datatypes)"


    get_recent(sys.argv[2])

    saved = set()
    uniquePageList = []
    for k in pageList:
        if k not in saved:
            uniquePageList.append(k)
            saved.add(k)

    # Save the new list
    save_file(sys.argv[1] + '.lst', '\n'.join(uniquePageList) + ('\n'
        if uniquePageList else ''))

    if Max:
        # Save the timestamp of the latest article
        Max = (datetime.datetime.strptime(Max, '%Y-%m-%dT%H:%M:%SZ')
               + datetime.timedelta(0, 1)).isoformat() + 'Z'
        save_file(sys.argv[1] + '.ts', Max)
    # else don't touch the file, or we risk overwriting the date


try:
    result = main()
except KeyboardInterrupt:
    result = 1

if result:
    sys.exit(result)
